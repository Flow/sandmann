// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace

import java.time.temporal.ChronoUnit
import java.time.Instant
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneId

object Time:
  def splitUsecsIntoMillisAndNanos(uSecs: Long) =
    val millis = uSecs / 1000
    val nanos = (uSecs - (millis * 1000)) * 1000
    (millis, nanos)

  def realtimeUsecsToInstant(realtimeUsecs: Long): Instant =
    val (millis, nanos) = splitUsecsIntoMillisAndNanos(realtimeUsecs)
    Instant.ofEpochMilli(millis).plusNanos(nanos)

  def monotonicUsecsToInstant(monotonicUsecs: Long): Instant =
    val (millis, nanos) = splitUsecsIntoMillisAndNanos(monotonicUsecs)
    Instant.now().plusMillis(millis).plusNanos(nanos)

  extension (offsetDateTime: OffsetDateTime)
    def forwardTo(localTime: LocalTime): OffsetDateTime =
      val adjustedTime = offsetDateTime.truncatedTo(ChronoUnit.DAYS).`with`(localTime)

      if adjustedTime.isBefore(offsetDateTime) then adjustedTime.plus(1, ChronoUnit.DAYS)
      else adjustedTime

  extension (instant: Instant)
    def forwardTo(localTime: LocalTime, zoneId: ZoneId = ZoneId.systemDefault()): Instant =
      val offsetDateTime = OffsetDateTime.ofInstant(instant, zoneId)
      offsetDateTime.forwardTo(localTime).toInstant
