// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.systemd

import scala.collection.mutable.Buffer
import scala.jdk.CollectionConverters.*

import java.time.Instant
import java.time.OffsetDateTime

import jnr.ffi.LibraryLoader
import jnr.ffi.Pointer
import org.freedesktop.dbus.connections.impl.DBusConnection
import org.freedesktop.dbus.interfaces.Properties
import org.freedesktop.dbus.types.UInt64
import org.freedesktop.dbus.types.Variant
import org.freedesktop.systemd1.ListUnitsByPatternsStruct
import org.freedesktop.systemd1.Manager

@SuppressWarnings(
  Array(
    "scalafix:DisableSyntax.null",
    "scalafix:DisableSyntax.asInstanceOf",
  )
)
object Systemd:
  trait Interface:
    def sd_notify(unset_environment: Int, state: String): Int

    def sd_watchdog_enabled(unset_environment: Int, usec: Pointer): Int

  val libSystemd = LibraryLoader.create(classOf[Interface]).load("systemd")

  val isRunningUnderSystemd =
    val res = libSystemd.sd_watchdog_enabled(0, null)
    if res < 0 then throw new Exception(s"sd_watchdog_enabled() returned error: $res")
    else res > 0

  def notifyReady() = if isRunningUnderSystemd then libSystemd.sd_notify(0, "READY=1")

  def notifyWatchdog() = if isRunningUnderSystemd then libSystemd.sd_notify(0, "WATCHDOG=1")

  object ActiveTimer:
    def apply(
        unit: ListUnitsByPatternsStruct,
        properties: Map[String, Variant[?]],
    ) =
      import eu.geekplace.Time
      val nextExecutionProperties = List(
        (
          "NextElapseUSecRealtime",
          Time.realtimeUsecsToInstant,
        ),
        (
          "NextElapseUSecMonotonic",
          Time.monotonicUsecsToInstant,
        ),
      )
      val nextExecutions = for
        (name, f) <- nextExecutionProperties
        untypedValue <- properties.get(name)
        value = untypedValue.getValue().asInstanceOf[UInt64].value()
        // Systemd will set the next execution to UInt64.MAX_BIG_VALUE
        // in some cases if the timer is never going to be run
        // (again).
        if value != UInt64.MAX_BIG_VALUE
        longValue = value.longValueExact()
        // Likewise the value may be 0 in certain situations. But here
        // we use the long type for comparision.
        if longValue != 0
        instant = f(longValue)
      yield instant
      val nextExecution = nextExecutions.sorted.headOption.getOrElse(Instant.MAX)

      val persistent = for
        untypedVariant <- properties.get("Persistent")
        value = untypedVariant.getValue().asInstanceOf[Boolean]
      yield value

      new ActiveTimer(unit, properties, nextExecution, persistent.get)

  case class ActiveTimer(
      unit: ListUnitsByPatternsStruct,
      properties: Map[String, Variant[?]],
      instant: Instant,
      persistent: Boolean,
  ):
    lazy val localTime = OffsetDateTime.ofInstant(instant, java.time.ZoneId.systemDefault())
    override def toString = s"${unit.getUnitName()} - ${instant}: p:${persistent}"
    def toVerboseString = s"${unit.getUnitName()} - ${instant}: ${properties}"
    def toLocalTzString = s"${unit.getUnitName()} - ${localTime}"

class Systemd(val sessionConnection: DBusConnection):
  val manager = sessionConnection.getRemoteObject(
    "org.freedesktop.systemd1",
    "/org/freedesktop/systemd1",
    classOf[Manager],
  )

  def getUnits() =
    manager.ListUnits()

  def getActiveTimers(): Buffer[Systemd.ActiveTimer] =
    val states = List("active").asJava
    val patterns = List("*.timer").asJava
    val activeTimers = manager.ListUnitsByPatterns(states, patterns).asScala

    for
      activeTimer <- activeTimers
      busPath = activeTimer.getObjectPath().toString()
      properties = sessionConnection
        .getRemoteObject("org.freedesktop.systemd1", busPath, classOf[Properties])
        .GetAll("org.freedesktop.systemd1.Timer")
        .asScala
        .toMap
    yield Systemd.ActiveTimer(activeTimer, properties)

  val TIMER_UNIT_EXT = ".timer"

  def getEarliestTimer(timers: Seq[String]): Option[Systemd.ActiveTimer] =
    val matchingTimers = getActiveTimers().filter(t =>
      val unitName = t._1.getUnitName()
      if unitName.endsWith(TIMER_UNIT_EXT) then
        val timerName = unitName.substring(0, unitName.length() - TIMER_UNIT_EXT.length())
        timers.contains(timerName)
      else false
    )

    matchingTimers.sortBy(_._3).headOption
