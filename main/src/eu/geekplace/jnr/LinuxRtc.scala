// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.jnr

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId

import jnr.constants.platform.OpenFlags.O_CLOEXEC
import jnr.constants.platform.OpenFlags.O_RDONLY
import linux.rtc.RTC_WKALM_SET
import linux.rtc.RtcWkalrm
import os.Path
import Linux.getErrnoInfoString

class LinuxRtc(rtcDev: Path = Path("/dev/rtc0"), rtcInUtc: Boolean = true)
    extends AutoCloseable
    with org.apache.logging.log4j.scala.Logging:

  val fd = Linux.posix.`open`(rtcDev.toString(), O_RDONLY.intValue() | O_CLOEXEC.intValue(), 0)
  if fd < 0 then throw new Exception(s"Could not open $rtcDev: ${getErrnoInfoString()} (${fd})")

  val rtcZone = if rtcInUtc then ZoneId.of("UTC") else ZoneId.systemDefault()

  // scalafix:off DisableSyntax.var
  var fdValid = true
  // scalafix:on

  def setAlarm(when: Instant) =
    if !fdValid then throw new Exception(s"File descriptor for $rtcDev already closed")

    val rtcDateTime = LocalDateTime.ofInstant(when, rtcZone)
    val rtcWkarlm = RtcWkalrm(rtcDateTime)
    val res = Linux.libc.ioctl(fd, RTC_WKALM_SET, rtcWkarlm)
    if res < 0 then
      throw new Exception(s"Could not set $rtcDev alarm to $when: ${getErrnoInfoString()} (${res})")

  override def close() =
    if fdValid then
      fdValid = false
      logger.debug(s"Closing file descriptor $fd for $rtcDev")
      val res = Linux.posix.close(fd)
      if res < 0 then
        throw new Exception(
          s"Failed to close ${rtcDev}'s file descriptor ${fd}: ${Linux.getErrnoInfoString()} (${res})"
        )
