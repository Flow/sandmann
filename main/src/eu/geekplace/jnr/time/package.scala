// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.jnr.time

import java.time.Instant
import java.time.OffsetDateTime

import eu.geekplace.jnr.Linux
import jnr.ffi.Runtime
import jnr.ffi.Struct

object Timespec:
  def apply(): Timespec =
    val zero = new Timespec(Linux.jnrRuntime)
    zero.seconds.set(0)
    zero.nanoseconds.set(0)
    zero

  def apply(offsetDateTime: OffsetDateTime): Timespec = apply(offsetDateTime.toInstant())
  def apply(instant: Instant): Timespec =
    val timespec = new Timespec(Linux.jnrRuntime)
    timespec.seconds.set(instant.getEpochSecond())
    timespec.nanoseconds.set(instant.getNano())
    timespec

  def apply(seconds: Long, nanoseconds: Long = 0, runtime: Runtime = Linux.jnrRuntime) =
    val timespec = new Timespec(runtime)
    timespec.seconds.set(seconds)
    timespec.nanoseconds.set(nanoseconds)
    timespec

  def zero(): Timespec = apply()

// See man timespec(3type)
final class Timespec(val runtime: Runtime) extends Struct(runtime):
  val seconds = time_t()
  val nanoseconds = SignedLong()

  def instant = Instant.ofEpochSecond(seconds.get(), nanoseconds.get())

  def set(other: Timespec) =
    seconds.set(other.seconds.get())
    nanoseconds.set(other.nanoseconds.get())

  override def toString() = s"Timespec(seconds=$seconds, nanoseconds=$nanoseconds [instant=$instant])"

object Itimerspec:
  def apply(interval: Timespec = Timespec.zero(), value: Timespec = Timespec.zero()): Itimerspec =
    val itimerspec = new Itimerspec(Linux.jnrRuntime)
    itimerspec.interval.set(interval)
    itimerspec.value.set(value)
    itimerspec

// See man itimerspec(3type)
final class Itimerspec(val runtime: Runtime) extends Struct(runtime):
  val interval = inner(classOf[Timespec])
  val value = inner(classOf[Timespec])

  override def toString() = s"Itimerspec(interval=$interval, value=$value)"
