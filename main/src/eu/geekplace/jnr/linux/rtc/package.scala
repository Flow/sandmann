// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.jnr.linux.rtc

import java.time.LocalDateTime

import eu.geekplace.jnr.Linux
import jnr.ffi.Runtime
import jnr.ffi.Struct

// #include <linux/rtc.h>

// WARNING: These ioctl numbers are probably platform and architecture
// dependent.
val RTC_WKALM_SET: Long = 0x4028700f
val RTC_WKALM_RD: Long = 0x80287010

object RtcTime:
  def apply(time: LocalDateTime, runtime: Runtime = Linux.jnrRuntime): RtcTime =
    val rtctime = new RtcTime(runtime)
    rtctime.sec.set(time.getSecond())
    rtctime.min.set(time.getMinute())
    rtctime.hour.set(time.getHour())
    rtctime.mday.set(time.getDayOfMonth())
    rtctime.mon.set(time.getMonthValue() - 1)
    rtctime.year.set(time.getYear() - 1900)
    rtctime.wday.set(-1)
    rtctime.yday.set(-1)
    rtctime.isdst.set(-1)
    rtctime

// See rtc(4) and tm(3type)
final class RtcTime(runtime: Runtime) extends Struct(runtime):
  val sec = Signed32()
  val min = Signed32()
  val hour = Signed32()
  val mday = Signed32()
  val mon = Signed32()
  val year = Signed32()
  val wday = Signed32()
  val yday = Signed32()
  val isdst = Signed32()

  def set(other: RtcTime) =
    sec.set(other.sec.get())
    min.set(other.min.get())
    hour.set(other.hour.get())
    mday.set(other.mday.get())
    mon.set(other.mon.get())
    year.set(other.year.get())
    wday.set(other.wday.get())
    yday.set(other.yday.get())
    isdst.set(other.isdst.get())

object RtcWkalrm:
  def apply(time: LocalDateTime, enabled: Boolean = true, runtime: Runtime = Linux.jnrRuntime): RtcWkalrm =
    val wake = new RtcWkalrm(runtime)
    if enabled then wake.enabled.set(1)
    wake.time.set(RtcTime(time, runtime))
    wake

// See rtc(4) for struct rtc_wkalrm
@SuppressWarnings(
  Array(
    "scalafix:DisableSyntax.var"
  )
)
final class RtcWkalrm(runtime: Runtime) extends Struct(runtime):
  val enabled = Unsigned8()
  val pending = Unsigned8()
  var time = inner(classOf[RtcTime])
