// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.jnr.linux

import java.time.Instant
import java.time.OffsetDateTime

import eu.geekplace.jnr.time.Itimerspec
import eu.geekplace.jnr.time.Timespec
import eu.geekplace.jnr.Linux

class RealtimeAlarm extends AutoCloseable with org.apache.logging.log4j.scala.Logging:
  import Linux.getErrnoInfoString

  val timerfd =
    val flags = Linux.TFD_CLOEXEC | Linux.TFD_NONBLOCK
    val clock = Linux.CLOCK_REALTIME_ALARM
    logger.debug(s"Creating timerfd with clock $clock and flags ${Integer.toBinaryString(flags)}")
    Linux.libc.timerfd_create(clock, flags)
  if timerfd < 0 then throw new Exception(s"Could not create timerfd: ${getErrnoInfoString()} (${timerfd})")
  else logger.debug(s"Created timerfd $timerfd")

  // scalafix:off DisableSyntax.var
  var fdValid = true
  // scalafix:on

  def setAlarm(when: OffsetDateTime): Boolean = setAlarm(when.toInstant())

  def setAlarm(when: Instant): Boolean =
    if !fdValid then throw new Exception("Timerfd was already closed")

    val alarmValue = Timespec(when)
    val flags = Linux.TFD_TIMER_ABSTIME
    val alarmInterval = Timespec.zero()
    val itimerspec = Itimerspec(alarmInterval, alarmValue)
    // TODO: Should we also enable TFD_TIMER_CANCEL_ON_SET to become
    // aware if the realtime clock undergoes a discontinuous change?
    // This would also require that we read from the timerfd to detect
    // ECANCELED being returned.
    logger.debug(s"Setting timerfd $timerfd with flags $flags to $itimerspec for $when")
    val res =
      Linux.libc.timerfd_settime(timerfd, flags, itimerspec, null) // scalafix:ok DisableSyntax.null
    if res < 0 then throw new Exception(s"Failed to set timerfd ${timerfd}: ${getErrnoInfoString()} (${res})")
    else true

  def getAlarm(): Itimerspec =
    val itimerspec = Itimerspec()
    val res = Linux.libc.timerfd_gettime(timerfd, itimerspec)
    if res < 0 then
      throw new Exception(s"Failed to get timerfd ${timerfd} time: ${getErrnoInfoString()} (${res})")
    itimerspec

  override def close() =
    if fdValid then
      fdValid = false
      logger.debug(s"Closing timerfd $timerfd")
      val res = Linux.posix.close(timerfd)
      if res < 0 then
        throw new Exception(s"Failed to close timerfd ${timerfd}: ${Linux.getErrnoInfoString()} (${res})")
