// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.jnr

import eu.geekplace.jnr.linux.rtc.RtcWkalrm
import eu.geekplace.jnr.time.Itimerspec
import eu.geekplace.jnr.time.Timespec
import jnr.ffi.LastError
import jnr.ffi.LibraryLoader
import jnr.ffi.Runtime

object Linux:
  // #include <include/time.h>
  val CLOCK_REALTIME = 0
  val CLOCK_MONOTONIC = 1
  val CLOCK_REALTIME_ALARM = 8
  val CLOCK_BOOTTIME_ALARM = 9

  // #include <bits/timerfd.h>
  // WARNING: Those are the common flag values. However some
  // architectures, like alpha, hppa, mips, and sparc have different
  // values.
  val TFD_CLOEXEC = 0x80000
  val TFD_NONBLOCK = 0x800

  // #include <sys/timerfd.h>
  val TFD_TIMER_ABSTIME = 1
  val TFD_TIMER_CANCEL_ON_SET = 2

  trait Interface:
    def timerfd_create(clockId: Int, flags: Int): Int
    def timerfd_settime(fd: Int, flags: Int, newValue: Itimerspec, oldValue: Itimerspec): Int
    def timerfd_gettime(fd: Int, curr_value: Itimerspec): Int

    // rtc(4)
    def ioctl(fd: Int, request: Long, wkarlm: RtcWkalrm): Int

    def nanosleep(req: Timespec, rem: Timespec): Int

    def strerrorname_np(errnum: Int): String
    def strerrordesc_np(errnum: Int): String

  val libc = LibraryLoader.create(classOf[Interface]).load("c")
  val jnrRuntime = Runtime.getRuntime(libc)

  def getErrnoInfo(): (Int, String, String) =
    val errno = LastError.getLastError(jnrRuntime)
    val name = libc.strerrorname_np(errno)
    val desc = libc.strerrordesc_np(errno)
    (errno, name, desc)

  def getErrnoInfoString() =
    val (errno, name, desc) = getErrnoInfo()
    s"${name}/${errno} - ${desc}"

  val posix = jnr.posix.POSIXFactory.getPOSIX()
