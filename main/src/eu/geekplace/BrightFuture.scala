// SPDX-License-Identifier: LGPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace

import scala.annotation.tailrec
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.concurrent.Promise
import scala.util.Either
import scala.util.Left
import scala.util.Right
import scala.util.Success
import scala.util.Try

import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger

object BrightFuture:
  final def join[T](futures: Seq[Future[T]])(implicit
      executor: ExecutionContext
  ): Future[Seq[Try[T]]] =
    val i = futures.iterator
    if !i.hasNext then Future.never
    else
      val p = Promise[Seq[Try[T]]]
      val counter = new AtomicInteger(Integer.MAX_VALUE)

      def decrement(delta: Int) =
        val prev = counter.getAndAdd(-delta)
        if prev == delta then
          // We set the counter to zero, no predicate matched, all
          // futures are finished, hence calling future.value.get is
          // safe.
          val values = futures.map(_.value.get)
          p tryComplete Success(values)

      // scalafix:off DisableSyntax.var
      var requiredSignalCount = 0
      // scalafix:on
      while i.hasNext do
        i.next().onComplete((_) => decrement(1))
        requiredSignalCount += 1

      val delta = Integer.MAX_VALUE - requiredSignalCount
      decrement(delta)

      p.future

  final def firstCompletedOf[T, U](futures: Seq[Future[T]])(
      predicate: PartialFunction[Try[T], U] = (t: Try[T]) =>
        t match
          case Success(t) => t
  )(implicit
      executor: ExecutionContext
  ): Future[Either[U, Seq[Try[T]]]] =
    val i = futures.iterator
    if !i.hasNext then Future(Right(Seq.empty))
    else
      val p = Promise[Either[U, Seq[Try[T]]]]()
      val counter = new AtomicInteger(Integer.MAX_VALUE)
      val predicateMatched = new AtomicBoolean(false)

      def decrement(delta: Int) =
        val prev = counter.getAndAdd(-delta)
        if prev == delta && !p.isCompleted then
          // We set the counter to zero, no predicate matched, all
          // futures are finished, hence calling future.value.get is
          // safe.
          val values = futures.map(_.value.get)
          p tryComplete Success(Right(values))

      val completeHandler = (t: Try[T]) =>
        val completed =
          if predicate isDefinedAt t then
            val predicateAlreadyMatched = predicateMatched `getAndSet` true
            if !predicateAlreadyMatched then
              val res = predicate(t)
              p tryComplete Success(Left(res))
              true
            else false
          else false
        if !completed then decrement(1)

      @tailrec
      def recurse(i: Iterator[Future[T]], count: Int): (Int, Boolean) =
        if i.hasNext then
          if predicateMatched.get() then (count, true)
          else
            i.next().onComplete(completeHandler)
            recurse(i, count + 1)
        else (count, false)

      val (requiredSignalCount, earlyExit) = recurse(i, 0)

      if !earlyExit then // optimization: only decrement if we did not exit early
        val delta = Integer.MAX_VALUE - requiredSignalCount
        decrement(delta)

      p.future
