// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023-2024 Florian Schmaus
package eu.geekplace.sandmann

import scala.annotation.tailrec
import scala.concurrent.blocking
import scala.concurrent.duration.*
import scala.concurrent.Await
import scala.concurrent.Future
import scala.util.chaining.*
import scala.util.Failure
import scala.util.Random
import scala.util.Success
import scala.util.Try
import scala.util.Using

import java.time.temporal.ChronoUnit
import java.time.DayOfWeek
import java.time.Duration
import java.time.Instant
import java.time.LocalTime
import java.time.OffsetDateTime

import eu.geekplace.jnr.linux.RealtimeAlarm
import eu.geekplace.jnr.LinuxRtc
import eu.geekplace.systemd.Systemd
import eu.geekplace.BrightFuture
import eu.geekplace.Time.forwardTo
import org.freedesktop.dbus.connections.impl.DBusConnection
import org.freedesktop.dbus.connections.impl.DBusConnectionBuilder

object Sandmann:

  Logging.initialize()

  val version: String = scala.io.Source.fromResource("eu.geekplace.sandmann/version").getLines.next
  val gitVersion: String = scala.io.Source.fromResource("eu.geekplace.sandmann/gitVersion").getLines.next
  val scalaVersion: String = scala.io.Source.fromResource("eu.geekplace.sandmann/scalaVersion").getLines.next

  val systemTimezone = java.time.ZoneId.systemDefault()

  implicit val ec: scala.concurrent.ExecutionContext = scala.concurrent.ExecutionContext.global

  def main(args: Array[String]): Unit =
    val specialCommand =
      if args.length == 3 then
        args(2) match
          case "repl" =>
            // ammonite
            //   .Main(
            //   )
            //   .run(
            //     // https://github.com/com-lihaoyi/Ammonite/issues/1250
            //     // "sandmann" -> Sandmann(config),
            //   )
            throw new Exception("Ammonite REPL not yet avaialble with Scala 3.3")
          case "list" =>
            list()
            true
          case "set-alarm-from-suspend" =>
            setAlarmFromSuspend()
            true
          case "set-alarm-from-poweroff" =>
            setAlarmFromPoweroff()
            true
          case "suspend" =>
            suspend()
            true
          case _ => false
      else false

    if !specialCommand then
      val config = Configuration(args.toIndexedSeq)
      Using.resource(DBusConnectionBuilder.forSystemBus().build()) { dbus =>
        Using.resource(Sandmann(config, dbus)) { sandmann => sandmann.run() }
      }

  private def suspend() =
    Using.resource(DBusConnectionBuilder.forSystemBus().build()) { sessionConnection =>
      val logind = Logind(sessionConnection, null) // scalafix:ok DisableSyntax.null
      logind.suspendThenHibernateSystem()
    }

  private def list() =
    Using.resource(DBusConnectionBuilder.forSystemBus().build()) { sessionConnection =>
      val logind = Logind(sessionConnection, null) // scalafix:ok DisableSyntax.null
      val sessions = logind.getSessionProperties()
      println("Sessions")
      println(sessions)

      println("System")
      val system = logind.getSystemProperties()
      println(system)

      println("Inhibitors")
      val inhibitors = logind.getInhibitors()
      println(inhibitors)

      val systemd = Systemd(sessionConnection)
      // println("Units")
      // val units = systemd.getUnits()
      // println(units)

      println("Sorted Timers")
      val activeTimers = systemd.getActiveTimers().sortBy(_.instant)
      println(activeTimers.map(_.toVerboseString).mkString("\n- ", "\n- ", ""))
    }

  private def setAlarmFromSuspend() =
    val delay = Duration.ofSeconds(90)
    val alarmInstant = Instant.now().plus(delay)
    Using.resource(RealtimeAlarm()) { alarm =>
      alarm.setAlarm(alarmInstant)
      val itimerspec = alarm.getAlarm()
      println(s"Wakeup alarm set to $itimerspec, sleeping so that timerfd is not closed")
      // TODO: Drop the toMillis() once Sandmann is Java 19 or higher.
      Thread.sleep(delay.plus(Duration.ofSeconds(30)).toMillis())
    }

  private def setAlarmFromPoweroff() =
    val alarmInstant = Instant.now().plus(Duration.ofMinutes(3))
    Using.resource(LinuxRtc()) { rtc =>
      rtc.setAlarm(alarmInstant)
    }

class Sandmann(
    val config: Configuration,
    val dbusConnection: DBusConnection,
) extends AutoCloseable
    with org.apache.logging.log4j.scala.Logging:
  import Sandmann.*
  Logging.configure(config.logLevel)

  private val systemd = Systemd(dbusConnection)
  private val logind = Logind(dbusConnection, this)
  private val activityCheckHookDir = ActivityCheckHookDir(this)
  private val realtimeAlarmOption: Option[RealtimeAlarm] = if pretend then None else Some(RealtimeAlarm())

  // scalafix:off DisableSyntax.var
  private var idleSince: Option[Instant] = None
  @volatile private var lastWakeup: Option[Instant] = None
  @volatile private var whenToHibernate: Option[OffsetDateTime] = None
  @volatile private var aboutToHibernate: Boolean = false
  // scalafix:on

  def run(): Unit =
    logger.info(
      s"Starting Sandmann ${version} (${gitVersion}) using Scala ${scalaVersion} (library ${scala.util.Properties.versionNumberString}) and Java runtime ${Runtime.version()}"
    )
    logger.debug(s"Sandmann configuration: $config")

    logind.addDelayAction(logindSleepStartHook, logindSleepStopHook, Logind.InhibitLockKind.Sleep)
    logind.addDelayAction(
      () => scheduleWakeupBeforeShutdown("poweroff"),
      () => (),
      Logind.InhibitLockKind.Shutdown,
    )

    Systemd.notifyReady()

    // TODO: Should use dbus-java AbstractPropertiesChangedHandler to get notified once the system gets idle.
    while true do
      Systemd.notifyWatchdog()

      performCheck()

      val checkInterval = config.checkInterval
      logger.debug(s"Sleeping for $checkInterval")
      Thread.sleep(checkInterval.toMillis())

  private def debug = config.debug
  private def pretend = config.cli.pretend()

  private def runHookDir(dirName: String) =
    val hookDir = os.root / "etc" / "sandmann" / s"${dirName}.d"
    if hookDir.toIO.isDirectory() then
      val hooks = os
        .list(hookDir)
        .filter(_.toIO.canExecute())
      logger.debug(s"Found hooks in $hookDir: $hooks")
      val futures = hooks
        .map { hook =>
          Future {
            logger.debug(s"Invoking $hook")
            val res = blocking {
              os.proc(hook)
                .call(propagateEnv = false, timeout = Duration.ofSeconds(30).toMillis)
            }

            val stdout = res.out.text()
            lazy val stdoutStderrMessage =
              val stderr = res.err.text()
              val stderrMessage =
                if stderr.isEmpty then ""
                else f"\nstderr:\n$stderr"
              if stdout.isEmpty && stderrMessage.isEmpty then " (no stdout/stderr)"
              else f"\nstdout:\n${stdout}${stderrMessage}"

            val exitStatus = res.exitCode
            if res.exitCode != 0 then
              throw new Exception(
                s"$hook: returned failure exit status $exitStatus.$stdoutStderrMessage"
              )
            logger.debug(
              s"$hook: returned hook result $exitStatus.$stdoutStderrMessage"
            )
          }
        }
      val joinFuture = BrightFuture.join(futures)
      val res = Await.result(joinFuture, 2.minutes)
      res
        .collect {
          _ match
            case Failure(e) => e
        }
        .foreach { logger.error(_) }

  private def runPreFreezeHooks() = runHookDir("pre-freeze")
  private def runPostFreezeHooks() = runHookDir("post-freeze")

  case class WakeupInstantResult(
      timer: Systemd.ActiveTimer,
      instant: Instant,
      randomizedDelaySecs: Long,
  ):
    val offsetDateTime = OffsetDateTime.ofInstant(instant, systemTimezone)
    override def toString(): String =
      s"timer wakeup at $offsetDateTime for $timer with ${randomizedDelaySecs}s random delay"

  private def getWakeupInstant(): Option[WakeupInstantResult] =
    def considerWakeupConstraints(activeTimer: Systemd.ActiveTimer): WakeupInstantResult =
      val timerOffsetDateTime = OffsetDateTime.ofInstant(activeTimer.instant, systemTimezone)

      // First, if the user configured a wakeupTime, then ensure that we wakeup the system after the
      // timer at the earliest wakeup time.
      val wakeupConsideredWakeupTime = config.file.wakeupTime match
        case None             => timerOffsetDateTime
        case Some(wakeupTime) => timerOffsetDateTime.forwardTo(wakeupTime)

      // Second, if the user configured wakeup days, then forward the wakeup time to the next allowed day.
      val wakeupConsiderdDaysOfWeek = config.file.wakeupDaysOfWeek match
        case None => wakeupConsideredWakeupTime
        case Some(daysOfWeek) =>
          @tailrec
          def forwardToAllowedDay(time: OffsetDateTime, daysOfWeek: Set[DayOfWeek]): OffsetDateTime =
            if daysOfWeek.contains(time.getDayOfWeek()) then time
            else
              val nextDay = time.plus(1, ChronoUnit.DAYS)
              forwardToAllowedDay(nextDay, daysOfWeek)

          forwardToAllowedDay(wakeupConsideredWakeupTime, daysOfWeek)

      val randomizedDelaySecs =
        config.file.wakeupTimeRandomizedDelay
          .map(d => Random.between(0, d.getSeconds()))
          .getOrElse(0L)

      val wakeupInstant = wakeupConsiderdDaysOfWeek
        .plusSeconds(randomizedDelaySecs)
        .toInstant()
      WakeupInstantResult(activeTimer, wakeupInstant, randomizedDelaySecs)

    // If the user configured wakeup timers, then use
    // those. Otherwise, get all active timers, filter the persistent
    // one and use the earliest.
    val earliestTimer = config.file.wakeupTimers
      .map(systemd.getEarliestTimer(_))
      .flatten
      .orElse(
        systemd.getActiveTimers().filter(_.persistent).sortBy(_.instant).headOption
      )

    earliestTimer
      .map(considerWakeupConstraints)

  def scheduleWakeupFromPoweroff(instant: Instant) =
    // When the system is going to shut down we can not use
    // timerfd because the timerfd would be cancelled once the fd
    // is closed (on process termination when shutting down). So
    // instead we have three options:
    // A) use rtcwake binary (requires sudo)
    // B) write to /sys/class/rtc/rtc0/wakealarm (requires root, not facl possible)
    // C) use ioctl(RTC), see man rtc (requires facl on /dev/rtc0)
    // In the following, we use C.
    Using.resource(LinuxRtc()) { rtc =>
      rtc.setAlarm(instant)
    }

  sealed trait WakeupFromSuspendKind
  case class WakeupForHibernation(time: OffsetDateTime) extends WakeupFromSuspendKind
  case class WakeupForTimer(wakeupInstantResult: WakeupInstantResult) extends WakeupFromSuspendKind

  private def logScheduledWakeup(w: WakeupInstantResult, wakeupFrom: String) =
    logger.info(
      s"Earliest matching timer: ${w.timer.toLocalTzString}, scheduling wakeup from $wakeupFrom at ${w.offsetDateTime}"
        + s" (may considering wakeup time ${config.file.wakeupTime}"
        + s" and ${w.randomizedDelaySecs} random delay secs (${config.file.wakeupTimeRandomizedDelay}))"
    )

  private def scheduleWakeupBeforeShutdown(shutdownKind: String): Unit =
    if config.wakeupFromHibernationOrPoweroff then
      getWakeupInstant().map { w =>
        logScheduledWakeup(w, shutdownKind)
        scheduleWakeupFromPoweroff(w.instant)
      }

  private def logindSleepStartHook() =
    logger.debug(f"logindSleepStartHook: aboutToHibernate=${aboutToHibernate}")
    if !aboutToHibernate then scheduleWakeupBeforeSleep()

  private def scheduleWakeupBeforeSleep(): Unit =
    val now = OffsetDateTime.now()
    val wakeupFromSuspendKind: WakeupFromSuspendKind =
      if !config.hibernate then
        getWakeupInstant().map(WakeupForTimer(_)) match
          case Some(w) => w
          case None    => return // scalafix:ok DisableSyntax.return
      else
        val wakeupForHibernation = now.plus(config.hibernationDelay)
        config.file.hibernationTimeRange match
          case None =>
            getWakeupInstant()
              .filter(_.offsetDateTime.isBefore(wakeupForHibernation))
              .map(WakeupForTimer(_))
              .getOrElse(WakeupForHibernation(wakeupForHibernation))
          case Some(hibernationTimeRange) =>
            if hibernationTimeRange.inTimeRange(now) then
              logger.debug(
                s"Current time $now is in hibernation $hibernationTimeRange, scheduling wakeup for hibernation at $wakeupForHibernation"
              )
              // Scheduling wakeup-for-hibernation after hibernationDelay,
              // from there, the system can decide if it will hibernate or do
              // something else.
              WakeupForHibernation(wakeupForHibernation)
            else
              val wakeupInstant = getWakeupInstant()
              logger.debug(
                s"Current time $now is outside hibernation $hibernationTimeRange, next wakeup instant is $wakeupInstant"
              )
              // Now it gets interesting. Assume we have the following three times:
              // - ET  : Earliest Timer (in the future)
              // - HT_S: Hibernation Time Range Start
              // - HT_E: Hibernation Time Range End
              // If ET < HT_S, then schedule wakeup at ET
              // If ET > HT_S && ET < HT_E, then schedule wakeup at HT_S, because we do not want to wakeup during the night.
              // If ET > HT_S && ET > HT_E, then schedule wakeup at HT_S.
              wakeupInstant match
                case Some(wakeupInstantResult) =>
                  val hibernationTimeRangeStartToday =
                    now.`with`(hibernationTimeRange.start)
                  if wakeupInstantResult.offsetDateTime.isBefore(hibernationTimeRangeStartToday) then
                    logger.debug(
                      s"Outside hibernation time range and wakeup instant ${wakeupInstantResult.offsetDateTime} is before hibernation time range start $hibernationTimeRangeStartToday, scheduling wakeup for timer $wakeupInstantResult"
                    )
                    WakeupForTimer(wakeupInstantResult)
                  else
                    val adjustedWakeup = now.forwardTo(hibernationTimeRange.start)
                    logger.debug(
                      s"Outside hibernation time range and wakeup instant ${wakeupInstantResult.offsetDateTime} is after hibernation time range start $hibernationTimeRangeStartToday, scheduling wakeup for timer $adjustedWakeup"
                    )
                    WakeupForHibernation(adjustedWakeup)
                case None => WakeupForHibernation(wakeupForHibernation)

    val whenToWakeupFromSleep = wakeupFromSuspendKind match
      case WakeupForHibernation(whenToWakeupForHibernation) =>
        whenToHibernate = Some(whenToWakeupForHibernation)
        logger.debug(s"Scheduling wakeup from suspend for hibernation at $whenToWakeupForHibernation")
        whenToWakeupForHibernation
      case WakeupForTimer(w) =>
        logScheduledWakeup(w, "sleep")
        w.offsetDateTime

    realtimeAlarmOption match
      case Some(realtimeAlarm) =>
        val alarmSet = realtimeAlarm.setAlarm(whenToWakeupFromSleep)
        if alarmSet then logger.info(s"System wakeup from sleep scheduled for $whenToWakeupFromSleep")
      case None => logger.info(s"Would schedule wakeup from sleep at $whenToWakeupFromSleep")

  /** This method is called when we resume after sleeping (suspended/hibernated).
    */
  private def logindSleepStopHook() =
    val wakeupTime = Instant.now()

    logger.debug(f"logindSleepStopHook: setting lastWakeup to $wakeupTime")
    lastWakeup = Some(wakeupTime)

    if aboutToHibernate then aboutToHibernate = false

    runPostFreezeHooks()

    // Idle here must mean idle and not inhibitied. Or does it? What
    // if we wakeup from suspend during the night and something
    // quickly grabs an inhibit lock? Then we would maybe not
    // hibernate during the night…

    // Solution: Query Linux for the reason of the last wakeup. Was it
    // the RTC timer? Then hibernate directly. Otherwise not.

    // Systemd's sleep.c does something different. It poll's the
    // timerfd. If poll returns POLLIN for the timerfd, then systemd
    // assumes that the wakeup was caused by the timerfd and hence
    // hibernates the system (that's the easy part).

    // However, it appears to be also possible that the poll timerfd
    // returns without POLLIN, even though we poll indefinetly. In
    // that case, systemd assumes that the timerfd was not the reason
    // why the system left suspend and does not hibernate.

    // Ok, aanother attempt. We save the time when we are plan to
    // hibernate. If we wake up before this time, then we now that it
    // must be a user triggered wakeup and do nothing. If we wake up
    // after this time, then we hibernate directly.

    sealed trait HibernationDecission:
      def hibernate: Boolean
      def explaination: String

    case class NoHibernationAfterSuspend() extends HibernationDecission:
      override def hibernate = false
      override def explaination = "No hibernation after suspend scheduled"

    case class HibernationTimeSet(whenToHibernate: OffsetDateTime) extends HibernationDecission:
      val now = OffsetDateTime.now()
      val shortlyAfterHibernationTime = whenToHibernate.plus(Duration.ofMinutes(3))
      val afterHibernationTime = now.isAfter(whenToHibernate)
      val beforeShortlyAfterHibernationTime = now.isBefore(shortlyAfterHibernationTime)
      override def hibernate =
        // The before-shortly-after-hibernation-time check is a
        // unfortunate workaround for the following case. The user
        // manually hibernates the system, .e.g., via "systemctl
        // hibernate". This will cause the systemd sleep start hook to
        // invoked (PrepareForSleep), which causes sandmann to set
        // whenToHibernate, even though, the system is already about
        // to hibernate. If the user later, e.g., on the next morning,
        // is waking the system, sandmann will find that it is past
        // whenToHibernate and cause the system to hibernate. The
        // correct solution ot this would be that systemd's, or rather
        // logind's, PrepareForSleep signal include information about
        // the sleep "kind" (suspend vs. hibernate).
        afterHibernationTime && beforeShortlyAfterHibernationTime
      override def explaination =
        if hibernate then
          s"Current time $now is after hibernation time $whenToHibernate (and within the slack time which ends $shortlyAfterHibernationTime): time to hibernate"
        else
          s"Current time $now is before hibernation time $whenToHibernate or after the slack time ending $shortlyAfterHibernationTime: user wakeup, not going to hibernate"

    val hibernationDecission = whenToHibernate match
      case None => NoHibernationAfterSuspend()
      case Some(i) =>
        whenToHibernate = None
        HibernationTimeSet(i)

    logger.debug(s"Hibernation decission after resuming from sleep: ${hibernationDecission.explaination}")
    if hibernationDecission.hibernate then
      if config.wakeupFromHibernationOrPoweroff then scheduleWakeupBeforeShutdown("hibernation")
      runPreFreezeHooks()
      // Hibernation via systemd/logind also invokes the sleep delay
      // inhibitor "hook", so we set this flag to not also schedule a
      // wakeup before *sleep*.
      aboutToHibernate = true
      Try(logind.hibernateSystem()) recover { throwable =>
        // A typical exception that could be thrown here is
        // "DBusExecutionException: Not enough suitable swap space for
        // hibernation available on compatible block devices and file
        // systems"
        logger.warn(s"Failed to hibernate system: $throwable", throwable)
        aboutToHibernate = false
      }
  end logindSleepStopHook

  private def idleAction(): Boolean =
    val (inhibitorKind, action, actionName) =
      val userSessionCount = logind.getUserSessionCount()
      if userSessionCount > 0 then
        (
          Logind.InhibitLockKind.Sleep,
          // TODO: If we ever have a configuration option, where the
          // "idle but user sessions" case can be selected to be
          // either suspend, suspendThenHibernate, or
          // suspendThenHibernateSystemd, then this option would be
          // switched here.
          () =>
            runPreFreezeHooks()
            logind.suspendSystem()
          ,
          "suspend then hibernate (via sandmann)",
        )
      else
        (
          Logind.InhibitLockKind.Shutdown,
          () => logind.shutdownSystem(),
          "shutdown",
        )

    val inhibitors = logind.getBlockingInhibitors(inhibitorKind)
    if inhibitors.isEmpty then
      if pretend then
        logger.info(s"Would have invoked idle action \"$actionName\"")
        // Pretend we invoked the idle action successfully by returning 'true'.
        true
      else
        logger.info(s"Invoking idle action \"$actionName\"")
        Try(action()) recover {
          // TODO: Distinguish between "expected" exceptions like
          // "org.freedesktop.dbus.exceptions.DBusExecutionException:
          // There's already a shutdown or sleep operation in
          // progress" and unexpected ones.
          throwable =>
            logger.info(s"Failed to invoke idle action: $throwable", throwable)
            false
        }
        true
    else
      val inhibitorsString = inhibitors.mkString("\n- ", "\n- ", "")
      logger.info(s"${inhibitorKind} inhibitors found, not going to ${action}: ${inhibitorsString}")
      false

  def extractIdleInformationAndLogFailures(
      nonActiveResults: Seq[Try[IdleCheck.Result]]
  ): IdleCheck.Idle | IdleCheck.IdleSince =
    val (success, failure) = nonActiveResults.partitionMap {
      case Success(s) => Left(s)
      case Failure(f) => Right(f)
    }

    val (idleResults, idleSinceResults) = success.partitionMap {
      case is: IdleCheck.IdleSince => Right(is)
      case i: IdleCheck.Idle       => Left(i)
      case illegal => throw new IllegalArgumentException(s"Illegal argument: Input seq contained: $illegal")
    }

    for f <- failure do logger.warn(s"Idle check failed: $f", f)

    idleSinceResults.sortBy(_.since).headOption.getOrElse(IdleCheck.Idle())

  private def performCheck(): Unit =
    val logindCheck: Future[IdleCheck.Result] = logind.checkIdle()
    val activityHooksCheck = activityCheckHookDir.checkIdle()

    val allChecks = Seq(
      logindCheck,
      activityHooksCheck,
    )

    // Reports Left(Active) if there is at least one active element,
    // otherwise, if there is at least one InhibitIdle element, then
    // reports Left(InhibitIdle), otherwise returns the list of
    // nonActiveIdleresults.
    // To do so, we issue all idle check and abort on the first
    // IdleCheck.Active result. If there is none, then we wait for all
    // idle checks to return and see if one of them was
    // IdleCheck.InhibitIdle, which we then return as Left
    // result. Otherwise all checks returned Idle results.
    val res: Either[IdleCheck.ActiveOrInhibited, Seq[Try[IdleCheck.Result]]] = {
      val defensive = false
      if defensive then
        val futureResults = allChecks.map(f => Try(Await.result(f, 5.minutes)))
        val activeResult = futureResults.collectFirst { case Success(a: IdleCheck.Active) =>
          a
        }
        activeResult match
          case Some(active) => Left(active)
          case None         => Right(futureResults)
      else
        val joinFuture: Future[Either[IdleCheck.Active, Seq[Try[IdleCheck.Result]]]] =
          BrightFuture.firstCompletedOf(allChecks) { case Success(a: IdleCheck.Active) =>
            a
          }
        Await.result(joinFuture, 5.minutes)
    } pipe (_ match
      case l @ Left(_) => l
      case r @ Right(nonActiveResults) =>
        nonActiveResults
          .collectFirst { case Success(inhibitIdle: IdleCheck.InhibitIdle) =>
            Left(inhibitIdle)
          }
          .getOrElse(r)
    )

    logger.debug(s"Idle check result: $res")

    res match
      case Left(active: IdleCheck.Active) =>
        idleSince = None
        logger.info(f"System is active: $active")
      case Left(inhibitIdle: IdleCheck.InhibitIdle) =>
        logger.info(f"System idle inhibited: $inhibitIdle")
      case Right(nonActiveResults) =>
        val idle: IdleCheck.Idle | IdleCheck.IdleSince = extractIdleInformationAndLogFailures(
          nonActiveResults
        )

        val idleStart = {
          idleSince match
            case None =>
              val newIdleSince = idle match
                case i: IdleCheck.IdleSince => i.since
                case _: IdleCheck.Idle      => Instant.now()
              idleSince = Some(newIdleSince)
              newIdleSince
            case Some(idleStart) => idleStart
        } pipe (i =>
          lastWakeup match
            case None => i
            // Ensure that the reported idle start time is not older
            // than wakeupTime, if lastWakeup is set.
            case Some(wakeupTime) => if i.isBefore(wakeupTime) then wakeupTime else i
        ) pipe (i =>
          // An idle period starting with the beginning of the unix
          // epoch is usually caused by a component that does not
          // correctly initialize its idle since value. See for
          // example https://github.com/systemd/systemd/issues/35163. Instead
          // report the system boot timestamp.
          if i.getEpochSecond() == 0 then
            val uptimeMillis = java.lang.management.ManagementFactory.getRuntimeMXBean().getUptime()
            val systemBootTimestamp = Instant.now().minusMillis(uptimeMillis)
            logger.warn(
              s"Probably invalid idle since start of unix epoch converted to system boot timestap $systemBootTimestamp"
            )
            systemBootTimestamp
          else i
        )

        val duration = Duration.between(idleStart, Instant.now())
        val idleDurationExceeded = duration.compareTo(config.idleDuration) > 0
        if idleDurationExceeded then
          logger.info(
            s"Idle duration of ${config.idleDuration} exceeded, system idle for $duration since ${OffsetDateTime
                .ofInstant(idleStart, systemTimezone)}"
          )
          // Checking the idle-inhibtor lock here is not really
          // necessary, as checking the IdleHint of the logind manager
          // also consideres any idle-inhibitor locks.
          val idleInhibitors = logind.getBlockingInhibitors(Logind.InhibitLockKind.Idle)
          if idleInhibitors.isEmpty then
            val idleActionInvoked = idleAction()
          else
            val inhibitorsString = idleInhibitors.mkString("\n -", "\n- ", "")
            logger.info("Idle inhibitors found, not invoking idle action. Inhibitors:\n$inhibitorsString")
        else
          logger.debug(
            s"Idle duration of ${config.idleDuration} not exceeded, system idle for $duration since ${OffsetDateTime
                .ofInstant(idleStart, systemTimezone)}"
          )

  override def close() =
    logind.close()
    dbusConnection.close()
    realtimeAlarmOption.map { _.close() }
