// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.sandmann

import java.time.Duration
import java.time.Instant

import os.Path

def toPath(path: String): Path = Path.expandUser(path, os.pwd)

private val percentageFormat =
  val percentageFormat = java.text.NumberFormat.getPercentInstance
  percentageFormat.setMaximumFractionDigits(2)
  percentageFormat

extension (d: Double) def asPercentage = percentageFormat.format(d)

extension (i: Instant) def toCurrentTimeZone = i.atZone(java.time.ZoneId.systemDefault())

extension (d: Duration) def shorterThan(other: Duration) = d.compareTo(other) < 0

extension (d: Duration) def longerThan(other: Duration) = d.compareTo(other) > 0
