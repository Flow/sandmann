// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.sandmann

import scala.jdk.CollectionConverters.*

import eu.geekplace.systemd.Systemd
import org.apache.logging.log4j.core.config.builder.api.ConfigurationBuilderFactory
import org.apache.logging.log4j.core.config.Configurator
import org.apache.logging.log4j.core.config.LoggerConfig
import org.apache.logging.log4j.Level

object Logging:

  val context =
    val builder = ConfigurationBuilderFactory
      .newConfigurationBuilder()
      .setConfigurationName("sandmann")
      .setPackages("de.bwaldvogel.log4j")
    // .setStatusLevel(Level.DEBUG)

    val consoleAppenderName = "console"
    val consoleAppender = builder
      .newAppender(consoleAppenderName, "Console")
    // .addAttribute("target", ConsoleAppender.Target.SYSTEM_OUT)
    val consolePattern = builder
      .newLayout("PatternLayout")
      .addAttribute("pattern", "%d{HH:mm:ss} %-5level: %msg [%t]%n%throwable")
    consoleAppender.add(consolePattern)
    builder.add(consoleAppender)

    val systemdJournalAppenderName = "journal"
    val systemdJournalAppender = builder
      .newAppender(systemdJournalAppenderName, "SystemdJournal")
      .addAttribute("logSource", true)
      .addAttribute("syslogIdentifier", "sandmann")
    builder.add(systemdJournalAppender)

    val appenderRef =
      val appenderName =
        if Systemd.isRunningUnderSystemd then systemdJournalAppenderName else consoleAppenderName
      println(s"Set root logger to $appenderName")
      builder.newAppenderRef(appenderName)
    val rootLogger = builder.newRootLogger(Level.INFO).add(appenderRef)
    builder.add(rootLogger)

    // println(s"Log XML:\n${builder.toXmlConfiguration()}")
    Configurator.initialize(this.getClass().getClassLoader(), builder.build())

  def initialize() =
    context.getRootLogger().debug(s"Initializing logging context=$context")

  def configure(logLevel: Configuration.LogLevel) =
    import org.apache.logging.log4j.Level
    val noisyLoggerLevel = if logLevel.isLessSpecificThan(Level.TRACE) then logLevel else Level.INFO

    val levelConfig = Map(
      "" -> logLevel,
      "org.freedesktop.dbus" -> noisyLoggerLevel,
      "com.rm5248.dbusjava" -> noisyLoggerLevel,
    ).toSeq.sortBy(_._1.length)

    val config = context.getConfiguration()

    def setLevel(loggerConfig: LoggerConfig, level: Configuration.LogLevel): Boolean =
      if loggerConfig.getLevel().equals(level) then false
      else
        loggerConfig.setLevel(level)
        true

    val initialLoggerResults =
      for
        (loggerName, loggerLevel) <- levelConfig
        loggerConfig = config.getLoggerConfig(loggerName)
      yield
        if loggerName.equals(loggerConfig.getName) then setLevel(loggerConfig, loggerLevel)
        else
          val newLoggerConfig = new LoggerConfig(loggerName, loggerLevel, true)
          config.addLogger(loggerName, newLoggerConfig)
          true

    val setLevelResults =
      for
        (loggerName, loggerConfig) <- config
          .getLoggers()
          .entrySet()
          .asScala
          .map(e => (e.getKey(), e.getValue()))
        desiredLevel = levelConfig.findLast((name, _) => loggerName.startsWith(name)).map(_._2).get
      yield setLevel(loggerConfig, desiredLevel)

    val updatedConfig = (setLevelResults ++ initialLoggerResults).foldLeft(false)(_ || _)
    if updatedConfig then context.updateLoggers()
