// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.sandmann

import scala.util.Failure
import scala.util.Success
import scala.util.Try

import java.time.DayOfWeek
import java.time.Duration
import java.time.LocalTime
import java.time.OffsetDateTime

import eu.geekplace.systemd.Systemd
import org.rogach.scallop.*
import org.virtuslab.yaml.*
import os.*

object Configuration:
  val logger = org.apache.logging.log4j.scala.Logger(getClass)

  val xdgProjectDirs = dev.dirs.ProjectDirectories.from(
    "eu.geekplace.sandmann",
    "Florian Schmaus",
    "sandmann",
  )

  val defaultConfigurationFile =
    val dir =
      if Systemd.isRunningUnderSystemd then Path("/etc/sandmann")
      else Path(xdgProjectDirs.configDir)
    dir / "sandmann.yaml"

  val osPathConverter = singleArgConverter[Path](s => toPath(s))

  def apply(args: Seq[String]): Configuration =
    val cli = Cli(args)
    // Use default configuration if configuration file does not exist and issue a info message
    val configurationFilePath = cli.configurationFile()
    val configurationFile =
      if os.isFile(configurationFilePath) then
        logger.info(s"Processed configuration file at $configurationFilePath")
        os.read(cli.configurationFile()).as[Configuration.File] match
          case Right(config)   => config
          case Left(yamlError) => throw new Exception(yamlError.msg)
      else
        logger.info(s"Configuration file at ${configurationFilePath} not found, using default configuration")
        File()

    Configuration(cli, configurationFile)

  // implicit val pathReader: ConfigReader[Path] = ConfigReader.fromString[Path](
  //   ConvertHelpers.catchReadError(toPath(_))
  // )

  implicit def yamlDecoderForPath: YamlDecoder[Path] = YamlDecoder { case s @ Node.ScalarNode(value, _) =>
    try Right(toPath(value))
    catch case e: Exception => Left(ConstructError.from(s"Cannot parse $value as Path", s, "Path"))
  }

  implicit def yamlDecoderForDuration: YamlDecoder[Duration] = YamlDecoder {
    case s @ Node.ScalarNode(value, _) =>
      try Right(Duration.parse(value))
      catch case e: Exception => Left(ConstructError.from(s"Cannot parse $value as Duration", s, "Duration"))
  }

  implicit def yamlDecoderForLocalTime: YamlDecoder[LocalTime] = YamlDecoder {
    case s @ Node.ScalarNode(value, _) =>
      try Right(LocalTime.parse(value))
      catch
        case e: Exception => Left(ConstructError.from(s"Cannot parse $value as LocalTime", s, "LocalTime"))
  }

  implicit def yamlDecoderForDayOfWeek: YamlDecoder[DayOfWeek] = YamlDecoder {
    case s @ Node.ScalarNode(value, _) =>
      try Right(DayOfWeek.valueOf(value))
      catch
        case e: Exception => Left(ConstructError.from(s"Cannot parse $value as DayOfWeek", s, "DayOfWeek"))
  }

  type LogLevel = org.apache.logging.log4j.Level

  implicit def yamlDecoderForLogLevel: YamlDecoder[LogLevel] = YamlDecoder {
    case s @ Node.ScalarNode(value, _) =>
      try Right(org.apache.logging.log4j.Level.getLevel(value))
      catch case e: Exception => Left(ConstructError.from(s"Cannot parse $value as LogLevel", s, "LogLevel"))
  }

  case class TimeRange(start: LocalTime, stop: LocalTime):
    val startIsBeforeStop = start.isBefore(stop)
    def inTimeRange(time: OffsetDateTime): Boolean = inTimeRange(LocalTime.from(time))
    def inTimeRange(time: LocalTime): Boolean =
      val afterStart = time.isAfter(start)
      val beforeStop = time.isBefore(stop)

      if startIsBeforeStop then afterStart && beforeStop
      else afterStart || beforeStop

  implicit def yamlDecoderForTimeRange: YamlDecoder[TimeRange] = YamlDecoder {
    case s @ Node.ScalarNode(value, _) =>
      val times = value.split("\\.\\.")
      if times.length != 2 then
        Left(
          ConstructError.from(
            s"Cannot parse $value as TimeRange, split resuled in more than 2 elements: ${times.mkString("|")}",
            s,
            "TimeRange",
          )
        )
      else
        try
          val start = LocalTime.parse(times(0))
          val stop = LocalTime.parse(times(1))
          Right(TimeRange(start, stop))
        catch
          case e: Exception => Left(ConstructError.from(s"Cannot parse $value as TimeRange", s, "TimeRange"))
  }

  enum WakeupFromPoweroffMethod:
    case RtcWake extends WakeupFromPoweroffMethod
    case Ioctl extends WakeupFromPoweroffMethod

  case class File(
      logLevel: Option[LogLevel] = None,
      stateDir: Option[Path] = None,
      idleDuration: Option[Duration] = None,
      checkInterval: Option[Duration] = None,
      wakeupTimers: Option[List[String]] = None,
      wakeupTime: Option[LocalTime] = None,
      wakeupTimeRandomizedDelay: Option[Duration] = None,
      wakeupDaysOfWeek: Option[Set[DayOfWeek]] = None,
      wakeupFromHibernationOrPoweroff: Option[Boolean] = None,
      hibernate: Option[Boolean] = None,
      hibernationTimeRange: Option[TimeRange] = None,
      hibernationDelay: Option[Duration] = None,
  ) derives YamlDecoder

  val defaultStateDir =
    if Systemd.isRunningUnderSystemd then os.root / "var" / "lib" / "sandmann"
    else Path(xdgProjectDirs.dataLocalDir)

case class Configuration(cli: Cli, file: Configuration.File):
  import org.apache.logging.log4j.Level
  import Configuration.*

  val logLevel: LogLevel =
    val cliDebug = cli.debug()
    if cliDebug >= 2 then Level.TRACE
    else if cliDebug == 1 then Level.DEBUG
    else file.logLevel.getOrElse(Level.INFO)

  val debug = logLevel.isMoreSpecificThan(Level.DEBUG)

  val stateDir: Option[Path] = file.stateDir match
    case None =>
      val writeTestDir = Configuration.defaultStateDir / ".writeTest"
      Try(os.makeDir.all(writeTestDir)) match
        case Success(_) =>
          os.remove(writeTestDir)
          Some(Configuration.defaultStateDir)
        case Failure(_) => None
    case x => x
  val idleDuration: Duration = file.idleDuration.getOrElse(Duration.ofMinutes(30))
  val checkInterval: Duration = file.checkInterval.getOrElse(Duration.ofMinutes(3))
  // sandmann.service has WatchdogSec=15, ensure that the main loop runs at least every 13 minutes.
  if checkInterval.longerThan(Duration.ofMinutes(13)) then
    throw new IllegalArgumentException("checkInterval must be no longer than 13 minutes")

  val wakeupFromHibernationOrPoweroff = file.wakeupFromHibernationOrPoweroff.getOrElse(true)

  val hibernate = file.hibernate.getOrElse(true)
  val hibernationDelay = file.hibernationDelay.getOrElse(Duration.ofHours(2))

class Cli(arguments: Seq[String]) extends ScallopConf(arguments):
  val debug = tally()

  val pretend = opt[Boolean](
    default = Some(false),
    short = 'p',
  )

  val configurationFile = opt[Path](
    default = Some(Configuration.defaultConfigurationFile)
  )(Configuration.osPathConverter)

  val sourceRoot = opt[Path](
    default = None
  )(Configuration.osPathConverter)

  verify()
