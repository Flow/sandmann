// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023-2024 Florian Schmaus
package eu.geekplace.sandmann

import scala.collection.*
import scala.collection.mutable.Buffer
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.jdk.CollectionConverters.*

import eu.geekplace.jnr.Linux
import eu.geekplace.Time
import org.freedesktop.dbus.connections.impl.DBusConnection
import org.freedesktop.dbus.interfaces.DBusSigHandler
import org.freedesktop.dbus.interfaces.Properties
import org.freedesktop.dbus.types.UInt64
import org.freedesktop.dbus.types.Variant
import org.freedesktop.dbus.FileDescriptor
import org.freedesktop.login1.ListInhibitorsStruct
import org.freedesktop.login1.ListSessionsStruct
import org.freedesktop.login1.Manager

@SuppressWarnings(
  Array(
    "scalafix:DisableSyntax.asInstanceOf"
  )
)
object Logind:
  sealed trait DelayableLock:
    val name: String

  enum InhibitLockKind(val name: String):
    case Shutdown extends InhibitLockKind("shutdown") with DelayableLock
    case Sleep extends InhibitLockKind("sleep") with DelayableLock
    case Idle extends InhibitLockKind("idle")
    case HandlePowerKey extends InhibitLockKind("handle-power-key")
    case HandleSuspendKey extends InhibitLockKind("handle-suspend-key")
    case HandleLidSwitch extends InhibitLockKind("handle-lid-switch")

  class Idleable(val properties: Map[String, Variant[?]]):
    val idleHint = properties("IdleHint").getValue().asInstanceOf[Boolean]
    val idleSinceHint =
      val idleSinceHintUsecs =
        properties("IdleSinceHint").getValue().asInstanceOf[UInt64].value().longValueExact()
      Time.realtimeUsecsToInstant(idleSinceHintUsecs)

  object SessionClass:
    if getClass.desiredAssertionStatus then
      // https://stackoverflow.com/a/24729587/194894
      val duplicates = values.map(_.name).toList.groupBy(identity).collect { case (x, List(_, _, _*)) => x }
      assert(duplicates.isEmpty, s"Enum names are not distinct: ${duplicates}")

    val map = values.map(c => c.name -> c).toMap
    def apply(s: String): SessionClass = map(s)

  // systemd src/login/logind-session.c session_class_table
  // systemd src/login/logind-session.h SESSSION_CLASS_CAN_IDLE for canIdle
  enum SessionClass(val name: String, val canIdle: Boolean = false):
    case User extends SessionClass("user", true)
    case UserEarly extends SessionClass("user-early", true)
    case UserIncomplete extends SessionClass("user-incomplete")
    case Greeter extends SessionClass("greeter", true)
    case LockScreen extends SessionClass("lock-screen")
    case Background extends SessionClass("background")
    case BackgroundLight extends SessionClass("background-light")
    case Manager extends SessionClass("manager")
    case ManagerEarly extends SessionClass("manager-early")

  class Session(session: ListSessionsStruct, properties: Map[String, Variant[?]])
      extends Idleable(properties):
    val sessionClass: SessionClass =
      val classString = properties("Class").getValue().asInstanceOf[String]
      SessionClass(classString)
    override def toString =
      s"${session.getSessionId()} u:${session.getUserName()} s:${session
          .getSeatId()} i:${idleHint} (${idleSinceHint}) p:${properties}"

  class System(properties: Map[String, Variant[?]]) extends Idleable(properties):
    override def toString =
      s"System i:${idleHint} (${idleSinceHint}) p:${properties}"

@SuppressWarnings(
  Array(
    "scalafix:DisableSyntax.asInstanceOf"
  )
)
class Logind(val sessionConnection: DBusConnection, sandmann: Sandmann)
    extends IdleCheck(sandmann)
    with AutoCloseable:
  val manager =
    sessionConnection.getRemoteObject("org.freedesktop.login1", "/org/freedesktop/login1", classOf[Manager])

  def getUserSessionCount() = getSessionProperties().filter(_.sessionClass == Logind.SessionClass.User).size

  def getSessionProperties(): Buffer[Logind.Session] =
    val sessions = manager.ListSessions().asScala
    for
      session <- sessions
      busPath = session.getSessionObjectPath().toString()
      properties = sessionConnection
        .getRemoteObject("org.freedesktop.login1", busPath, classOf[Properties])
        .GetAll("org.freedesktop.login1.Session")
        .asScala
        .toMap // convert to immutable Map
    yield Logind.Session(session, properties)

  def getSystemProperties(): Logind.System =
    val properties = sessionConnection
      .getRemoteObject("org.freedesktop.login1", "/org/freedesktop/login1", classOf[Properties])
      .GetAll("org.freedesktop.login1.Manager")
      .asScala
      .toMap
    Logind.System(properties)

  def suspendSystem() = manager.Suspend(false)

  def hibernateSystem() = manager.Hibernate(false)

  def suspendThenHibernateSystem() = manager.SuspendThenHibernate(false)

  def shutdownSystem() = manager.PowerOff(false)

  def getBlockingInhibitors(kind: Logind.InhibitLockKind): List[ListInhibitorsStruct] =
    getInhibitors()
      .filter(_.getMode() == "block")
      .filter(_.getWhat().contains(kind.name))

  def getInhibitors(): List[ListInhibitorsStruct] = manager.ListInhibitors().asScala.toList

  override def checkIdle()(implicit ec: ExecutionContext): Future[IdleCheck.Result] = Future {
    // It is sufficient to just check the manager's object
    // IdleHint, as it is only true if all sessions are idle (or if
    // there are no sessions) and if no idle-inhibit lock is
    // active. See
    // https://github.com/systemd/systemd/blob/fd0a80427122c7bca0efb81ecc7ec11a483b4e6c/src/login/logind-core.c#L395-L431
    val system = getSystemProperties()
    val res = if system.idleHint then
      logger.debug(s"Logind reports system idle, returning Active\nSystem: $system")
      IdleCheck.IdleSince(system.idleSinceHint)
    else
      logger.debug(s"Logind reports system not idle, returning Active\nSystem: $system")
      IdleCheck.Active()

    if debug then
      val sessions = getSessionProperties()
      if sessions.isEmpty then logger.debug("Logind reports no user sessions")
      else
        logger.debug(s"Logind reports the following sessions: ${sessions.mkString("\n- ", "\n- ", "")}")
        val (idleSessions, activeSessions) = sessions.filter(_.sessionClass.canIdle).partition(_.idleHint)
        if activeSessions.isEmpty then
          val youngestIdleSession = idleSessions.sortBy(_.idleSinceHint).reverse.head
          logger.debug(
            s"Logind reports no active sessions, returning IdleSince.\nYoungest idle session: $youngestIdleSession"
          )
        else
          logger.debug(
            s"Logind reports active sessions, returning Active\nActive sessions: ${activeSessions
                .mkString("\n- ", "\n- ", "")}"
          )

    res
  }

  private val inhibitors = mutable.Map.empty[Logind.DelayableLock, FileDescriptor]
  private val signalHandles = mutable.Map.empty[Logind.DelayableLock, AutoCloseable]

  def addDelayAction(action: () => Unit, leaveAction: () => Unit, kind: Logind.DelayableLock) =
    val inhibitor = () => inhibitors.get(kind)
    if inhibitor().isDefined then throw new Exception(s"${kind.name} delay action already set")

    val takeInhibitor = () =>
      if inhibitor().isEmpty then
        val fd = manager.Inhibit(kind.name, "Sandmann", s"Perform actions before ${kind.name}", "delay")
        inhibitors(kind) = fd

    val signalClass = kind match
      case Logind.InhibitLockKind.Sleep    => classOf[Manager.PrepareForSleep]
      case Logind.InhibitLockKind.Shutdown => classOf[Manager.PrepareForShutdown]

    val signalHandler: DBusSigHandler[Manager.PrepareForSleep | Manager.PrepareForShutdown] =
      (prepare: Manager.PrepareForSleep | Manager.PrepareForShutdown) =>
        val start = prepare match
          case p: Manager.PrepareForSleep    => p.getStart()
          case p: Manager.PrepareForShutdown => p.getStart()
        logger.info(s"Received ${kind.name} signal: start=$start")
        if start then
          action()
          inhibitor().map(_.getIntFileDescriptor).map { fd =>
            val res = Linux.posix.close(fd)
            if res < 0 then
              logger.warn(s"Failed to close inhibitor fd $fd: ${Linux.getErrnoInfoString()} (${res})")
            inhibitors.remove(kind)
          }
        else
          try
            takeInhibitor()
          finally
            leaveAction()

    // val busName = sessionConnection.getNameOwner("org.freedesktop.login1")
    val signalHandle = kind match
      case Logind.InhibitLockKind.Sleep =>
        sessionConnection.addSigHandler(
          classOf[Manager.PrepareForSleep],
//          busName,
          signalHandler.asInstanceOf[DBusSigHandler[Manager.PrepareForSleep]],
        )
      case Logind.InhibitLockKind.Shutdown =>
        sessionConnection.addSigHandler(
          classOf[Manager.PrepareForShutdown],
//          busName,
          signalHandler.asInstanceOf[DBusSigHandler[Manager.PrepareForShutdown]],
        )
    signalHandles(kind) = signalHandle

    takeInhibitor()

  override def close() =
    inhibitors.values.map(_.getIntFileDescriptor).foreach { fd =>
      val res = Linux.posix.close(fd)
      if res < 0 then logger.warn(s"Failed to close inhibitor fd: ${Linux.getErrnoInfoString()} (${res})")
    }
    inhibitors.clear()

    signalHandles.values.foreach(_.close())
    signalHandles.clear()
