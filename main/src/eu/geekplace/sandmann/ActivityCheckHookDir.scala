// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.sandmann

import scala.concurrent.blocking
import scala.concurrent.ExecutionContext
import scala.concurrent.Future
import scala.util.Random
import scala.util.Success
import scala.util.Try

import java.time.Duration

import eu.geekplace.BrightFuture

class ActivityCheckHookDir(sandmann: Sandmann) extends IdleCheck(sandmann):
  val activityCheckHookDir = os.root / "etc" / "sandmann" / "activity-check.d"
  val sandmannLibexecDir =
    config.cli.sourceRoot.map(_ / "libexec").getOrElse(os.root / "usr" / "libexec" / "sandmann")

  override def checkIdle()(implicit ec: ExecutionContext): Future[IdleCheck.Result] =
    if !activityCheckHookDir.toIO.isDirectory() then
      logger.debug(s"Hook dir $activityCheckHookDir does not exist, returning idle")
      Future(IdleCheck.Idle())
    else
      val envPathOrig = sys.env.get("PATH").getOrElse("")
      val envPathNew = s"$sandmannLibexecDir:$envPathOrig"
      val token = Random.alphanumeric.take(12).mkString

      val idleMatch = s"SANDMANN $token: idle"
      val activeMatch = s"SANDMANN $token: active"
      val inhibitIdleMatch = s"SANDMANN $token: inhibit-idle"

      val env = Map(
        "PATH" -> envPathNew,
        "SANDMANN_TOKEN" -> token,
      )

      // val transformer = if debug then id else view
      val futures: Seq[Future[IdleCheck.Result]] = os
        .list(activityCheckHookDir)
        .filter(_.toIO.canExecute())
        .view
        .map { hook =>
          Future {
            logger.debug(s"Invoking $hook")

            val res = blocking {
              os
                .proc(hook)
                .call(env = env, propagateEnv = false, timeout = Duration.ofMinutes(1).toMillis)
            }

            val stdout = res.out.text()
            lazy val stdoutStderrMessage =
              val stderr = res.err.text()
              val stderrMessage =
                if stderr.isEmpty then ""
                else f"\nstderr:\n$stderr"
              if stdout.isEmpty && stderrMessage.isEmpty then " (no stdout/stderr)"
              else f"\nstdout:\n${stdout}${stderrMessage}"

            val exitStatus = res.exitCode
            if res.exitCode != 0 then
              throw new Exception(
                s"$hook: returned failure exit status $exitStatus.$stdoutStderrMessage"
              )
            val hookResult =
              stdout.linesIterator.toSeq.reverse.collectFirst {
                case line if line.equals(activeMatch)      => IdleCheck.Active()
                case line if line.equals(inhibitIdleMatch) => IdleCheck.InhibitIdle()
                case line if line.equals(idleMatch)        => IdleCheck.Idle()
              }
            logger.debug(
              s"$hook: returned hook result $hookResult.$stdoutStderrMessage"
            )
            hookResult.getOrElse(
              throw new Exception(s"$hook: did not include any activity-check marker.$stdoutStderrMessage")
            )
          }
        }
        .toSeq

      val future: Future[Either[IdleCheck.ActiveOrInhibited, Seq[Try[IdleCheck.Result]]]] =
        BrightFuture.firstCompletedOf(futures) { case Success(a: IdleCheck.ActiveOrInhibited) => a }
      future.map {
        case Left(active)            => active
        case Right(nonActiveResults) => sandmann.extractIdleInformationAndLogFailures(nonActiveResults)
      }
