// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.sandmann

import scala.concurrent.ExecutionContext
import scala.concurrent.Future

import java.time.Instant

object IdleCheck:
  sealed abstract class Result

  sealed abstract class ActiveOrInhibited extends Result
  class Active extends ActiveOrInhibited
  class InhibitIdle extends ActiveOrInhibited
  class Idle extends Result
  class IdleSince(val since: Instant) extends Idle

abstract class IdleCheck(val sandmann: Sandmann) extends org.apache.logging.log4j.scala.Logging:
  def config = sandmann.config
  def debug = config.debug
  def checkIdle()(implicit ec: ExecutionContext): Future[IdleCheck.Result]
