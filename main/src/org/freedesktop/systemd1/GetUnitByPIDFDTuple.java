package org.freedesktop.systemd1;

import java.util.List;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.Tuple;
import org.freedesktop.dbus.annotations.Position;

/**
 * Auto-generated class.
 */
public class GetUnitByPIDFDTuple extends Tuple {
    @Position(0)
    private DBusPath unit;
    @Position(1)
    private String unitId;
    @Position(2)
    private List<Byte> invocationId;

    public GetUnitByPIDFDTuple(DBusPath unit, String unitId, List<Byte> invocationId) {
        this.unit = unit;
        this.unitId = unitId;
        this.invocationId = invocationId;
    }

    public void setUnit(DBusPath arg) {
        unit = arg;
    }

    public DBusPath getUnit() {
        return unit;
    }
    public void setUnitId(String arg) {
        unitId = arg;
    }

    public String getUnitId() {
        return unitId;
    }
    public void setInvocationId(List<Byte> arg) {
        invocationId = arg;
    }

    public List<Byte> getInvocationId() {
        return invocationId;
    }


}