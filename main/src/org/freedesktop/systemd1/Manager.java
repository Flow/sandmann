package org.freedesktop.systemd1;

import java.util.List;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.FileDescriptor;
import org.freedesktop.dbus.TypeRef;
import org.freedesktop.dbus.annotations.DBusProperty;
import org.freedesktop.dbus.annotations.DBusProperty.Access;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.messages.DBusSignal;
import org.freedesktop.dbus.types.UInt32;
import org.freedesktop.dbus.types.UInt64;

/**
 * Auto-generated class.
 */
@DBusProperty(name = "Version", type = String.class, access = Access.READ)
@DBusProperty(name = "Features", type = String.class, access = Access.READ)
@DBusProperty(name = "Virtualization", type = String.class, access = Access.READ)
@DBusProperty(name = "Architecture", type = String.class, access = Access.READ)
@DBusProperty(name = "Tainted", type = String.class, access = Access.READ)
@DBusProperty(name = "FirmwareTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "FirmwareTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "LoaderTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "LoaderTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "KernelTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "KernelTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UserspaceTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UserspaceTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "FinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "FinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "SecurityStartTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "SecurityStartTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "SecurityFinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "SecurityFinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "GeneratorsStartTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "GeneratorsStartTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "GeneratorsFinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "GeneratorsFinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UnitsLoadStartTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UnitsLoadStartTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UnitsLoadFinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UnitsLoadFinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UnitsLoadTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UnitsLoadTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDSecurityStartTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDSecurityStartTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDSecurityFinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDSecurityFinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDGeneratorsStartTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDGeneratorsStartTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDGeneratorsFinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDGeneratorsFinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDUnitsLoadStartTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDUnitsLoadStartTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDUnitsLoadFinishTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InitRDUnitsLoadFinishTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "LogLevel", type = String.class, access = Access.READ_WRITE)
@DBusProperty(name = "LogTarget", type = String.class, access = Access.READ_WRITE)
@DBusProperty(name = "NNames", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "NFailedUnits", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "NJobs", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "NInstalledJobs", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "NFailedJobs", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "Progress", type = Double.class, access = Access.READ)
@DBusProperty(name = "Environment", type = Manager.PropertyEnvironmentType.class, access = Access.READ)
@DBusProperty(name = "ConfirmSpawn", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "ShowStatus", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "UnitPath", type = Manager.PropertyUnitPathType.class, access = Access.READ)
@DBusProperty(name = "DefaultStandardOutput", type = String.class, access = Access.READ)
@DBusProperty(name = "DefaultStandardError", type = String.class, access = Access.READ)
@DBusProperty(name = "WatchdogDevice", type = String.class, access = Access.READ)
@DBusProperty(name = "WatchdogLastPingTimestamp", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "WatchdogLastPingTimestampMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "RuntimeWatchdogUSec", type = UInt64.class, access = Access.READ_WRITE)
@DBusProperty(name = "RuntimeWatchdogPreUSec", type = UInt64.class, access = Access.READ_WRITE)
@DBusProperty(name = "RuntimeWatchdogPreGovernor", type = String.class, access = Access.READ_WRITE)
@DBusProperty(name = "RebootWatchdogUSec", type = UInt64.class, access = Access.READ_WRITE)
@DBusProperty(name = "KExecWatchdogUSec", type = UInt64.class, access = Access.READ_WRITE)
@DBusProperty(name = "ServiceWatchdogs", type = Boolean.class, access = Access.READ_WRITE)
@DBusProperty(name = "ControlGroup", type = String.class, access = Access.READ)
@DBusProperty(name = "SystemState", type = String.class, access = Access.READ)
@DBusProperty(name = "ExitCode", type = Byte.class, access = Access.READ)
@DBusProperty(name = "DefaultTimerAccuracyUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultTimeoutStartUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultTimeoutStopUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultTimeoutAbortUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultDeviceTimeoutUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultRestartUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultStartLimitIntervalUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultStartLimitBurst", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "DefaultCPUAccounting", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "DefaultBlockIOAccounting", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "DefaultMemoryAccounting", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "DefaultTasksAccounting", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitCPU", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitCPUSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitFSIZE", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitFSIZESoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitDATA", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitDATASoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitSTACK", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitSTACKSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitCORE", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitCORESoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitRSS", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitRSSSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitNOFILE", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitNOFILESoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitAS", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitASSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitNPROC", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitNPROCSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitMEMLOCK", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitMEMLOCKSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitLOCKS", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitLOCKSSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitSIGPENDING", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitSIGPENDINGSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitMSGQUEUE", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitMSGQUEUESoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitNICE", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitNICESoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitRTPRIO", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitRTPRIOSoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitRTTIME", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultLimitRTTIMESoft", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultTasksMax", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "TimerSlackNSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "DefaultOOMPolicy", type = String.class, access = Access.READ)
@DBusProperty(name = "DefaultOOMScoreAdjust", type = Integer.class, access = Access.READ)
@DBusProperty(name = "CtrlAltDelBurstAction", type = String.class, access = Access.READ)
public interface Manager extends DBusInterface {


    public DBusPath GetUnit(String name);
    public DBusPath GetUnitByPID(UInt32 pid);
    public DBusPath GetUnitByInvocationID(List<Byte> invocationId);
    public DBusPath GetUnitByControlGroup(String cgroup);
    public GetUnitByPIDFDTuple GetUnitByPIDFD(FileDescriptor pidfd);
    public DBusPath LoadUnit(String name);
    public DBusPath StartUnit(String name, String mode);
    public DBusPath StartUnitWithFlags(String name, String mode, UInt64 flags);
    public DBusPath StartUnitReplace(String oldUnit, String newUnit, String mode);
    public DBusPath StopUnit(String name, String mode);
    public DBusPath ReloadUnit(String name, String mode);
    public DBusPath RestartUnit(String name, String mode);
    public DBusPath TryRestartUnit(String name, String mode);
    public DBusPath ReloadOrRestartUnit(String name, String mode);
    public DBusPath ReloadOrTryRestartUnit(String name, String mode);
    public EnqueueUnitJobTuple EnqueueUnitJob(String name, String jobType, String jobMode);
    public void KillUnit(String name, String whom, int signal);
    public void CleanUnit(String name, List<String> mask);
    public void FreezeUnit(String name);
    public void ThawUnit(String name);
    public void ResetFailedUnit(String name);
    public void SetUnitProperties(String name, boolean runtime, List<SetUnitPropertiesStruct> properties);
    public void BindMountUnit(String name, String source, String destination, boolean readOnly, boolean mkdir);
    public void MountImageUnit(String name, String source, String destination, boolean readOnly, boolean mkdir, List<MountImageUnitStruct> options);
    public void RefUnit(String name);
    public void UnrefUnit(String name);
    public DBusPath StartTransientUnit(String name, String mode, List<StartTransientUnitStruct> properties, List<StartTransientUnitStruct> aux);
    public List<GetUnitProcessesStruct> GetUnitProcesses(String name);
    public void AttachProcessesToUnit(String unitName, String subcgroup, List<UInt32> pids);
    public void AbandonScope(String name);
    public DBusPath GetJob(UInt32 id);
    public List<GetJobAfterStruct> GetJobAfter(UInt32 id);
    public List<GetJobBeforeStruct> GetJobBefore(UInt32 id);
    public void CancelJob(UInt32 id);
    public void ClearJobs();
    public void ResetFailed();
    public void SetShowStatus(String mode);
    public List<ListUnitsStruct> ListUnits();
    public List<ListUnitsFilteredStruct> ListUnitsFiltered(List<String> states);
    public List<ListUnitsByPatternsStruct> ListUnitsByPatterns(List<String> states, List<String> patterns);
    public List<ListUnitsByNamesStruct> ListUnitsByNames(List<String> names);
    public List<ListJobsStruct> ListJobs();
    public void Subscribe();
    public void Unsubscribe();
    public String Dump();
    public String DumpUnitsMatchingPatterns(List<String> patterns);
    public FileDescriptor DumpByFileDescriptor();
    public FileDescriptor DumpUnitsMatchingPatternsByFileDescriptor(List<String> patterns);
    public void Reload();
    public void Reexecute();
    public void Exit();
    public void Reboot();
    public void PowerOff();
    public void Halt();
    public void KExec();
    public void SwitchRoot(String newRoot, String init);
    public void SetEnvironment(List<String> assignments);
    public void UnsetEnvironment(List<String> names);
    public void UnsetAndSetEnvironment(List<String> names, List<String> assignments);
    public List<DBusPath> EnqueueMarkedJobs();
    public List<ListUnitFilesStruct> ListUnitFiles();
    public List<ListUnitFilesByPatternsStruct> ListUnitFilesByPatterns(List<String> states, List<String> patterns);
    public String GetUnitFileState(String file);
    public EnableUnitFilesTuple EnableUnitFiles(List<String> files, boolean runtime, boolean force);
    public List<DisableUnitFilesStruct> DisableUnitFiles(List<String> files, boolean runtime);
    public EnableUnitFilesWithFlagsTuple EnableUnitFilesWithFlags(List<String> files, UInt64 flags);
    public List<DisableUnitFilesWithFlagsStruct> DisableUnitFilesWithFlags(List<String> files, UInt64 flags);
    public DisableUnitFilesWithFlagsAndInstallInfoTuple DisableUnitFilesWithFlagsAndInstallInfo(List<String> files, UInt64 flags);
    public ReenableUnitFilesTuple ReenableUnitFiles(List<String> files, boolean runtime, boolean force);
    public List<LinkUnitFilesStruct> LinkUnitFiles(List<String> files, boolean runtime, boolean force);
    public PresetUnitFilesTuple PresetUnitFiles(List<String> files, boolean runtime, boolean force);
    public PresetUnitFilesWithModeTuple PresetUnitFilesWithMode(List<String> files, String mode, boolean runtime, boolean force);
    public List<MaskUnitFilesStruct> MaskUnitFiles(List<String> files, boolean runtime, boolean force);
    public List<UnmaskUnitFilesStruct> UnmaskUnitFiles(List<String> files, boolean runtime);
    public List<RevertUnitFilesStruct> RevertUnitFiles(List<String> files);
    public List<SetDefaultTargetStruct> SetDefaultTarget(String name, boolean force);
    public String GetDefaultTarget();
    public List<PresetAllUnitFilesStruct> PresetAllUnitFiles(String mode, boolean runtime, boolean force);
    public List<AddDependencyUnitFilesStruct> AddDependencyUnitFiles(List<String> files, String target, String type, boolean runtime, boolean force);
    public List<String> GetUnitFileLinks(String name, boolean runtime);
    public void SetExitCode(byte number);
    public UInt32 LookupDynamicUserByName(String name);
    public String LookupDynamicUserByUID(UInt32 uid);
    public List<GetDynamicUsersStruct> GetDynamicUsers();


    public static interface PropertyEnvironmentType extends TypeRef<List<String>> {




    }

    public static interface PropertyUnitPathType extends TypeRef<List<String>> {




    }

    public static class UnitNew extends DBusSignal {

        private final String id;
        private final DBusPath unit;

        public UnitNew(String _path, String _id, DBusPath _unit) throws DBusException {
            super(_path, _id, _unit);
            this.id = _id;
            this.unit = _unit;
        }


        public String getId() {
            return id;
        }

        public DBusPath getUnit() {
            return unit;
        }


    }

    public static class UnitRemoved extends DBusSignal {

        private final String id;
        private final DBusPath unit;

        public UnitRemoved(String _path, String _id, DBusPath _unit) throws DBusException {
            super(_path, _id, _unit);
            this.id = _id;
            this.unit = _unit;
        }


        public String getId() {
            return id;
        }

        public DBusPath getUnit() {
            return unit;
        }


    }

    public static class JobNew extends DBusSignal {

        private final UInt32 id;
        private final DBusPath job;
        private final String unit;

        public JobNew(String _path, UInt32 _id, DBusPath _job, String _unit) throws DBusException {
            super(_path, _id, _job, _unit);
            this.id = _id;
            this.job = _job;
            this.unit = _unit;
        }


        public UInt32 getId() {
            return id;
        }

        public DBusPath getJob() {
            return job;
        }

        public String getUnit() {
            return unit;
        }


    }

    public static class JobRemoved extends DBusSignal {

        private final UInt32 id;
        private final DBusPath job;
        private final String unit;
        private final String result;

        public JobRemoved(String _path, UInt32 _id, DBusPath _job, String _unit, String _result) throws DBusException {
            super(_path, _id, _job, _unit, _result);
            this.id = _id;
            this.job = _job;
            this.unit = _unit;
            this.result = _result;
        }


        public UInt32 getId() {
            return id;
        }

        public DBusPath getJob() {
            return job;
        }

        public String getUnit() {
            return unit;
        }

        public String getResult() {
            return result;
        }


    }

    public static class StartupFinished extends DBusSignal {

        private final UInt64 firmware;
        private final UInt64 loader;
        private final UInt64 kernel;
        private final UInt64 initrd;
        private final UInt64 userspace;
        private final UInt64 total;

        public StartupFinished(String _path, UInt64 _firmware, UInt64 _loader, UInt64 _kernel, UInt64 _initrd, UInt64 _userspace, UInt64 _total) throws DBusException {
            super(_path, _firmware, _loader, _kernel, _initrd, _userspace, _total);
            this.firmware = _firmware;
            this.loader = _loader;
            this.kernel = _kernel;
            this.initrd = _initrd;
            this.userspace = _userspace;
            this.total = _total;
        }


        public UInt64 getFirmware() {
            return firmware;
        }

        public UInt64 getLoader() {
            return loader;
        }

        public UInt64 getKernel() {
            return kernel;
        }

        public UInt64 getInitrd() {
            return initrd;
        }

        public UInt64 getUserspace() {
            return userspace;
        }

        public UInt64 getTotal() {
            return total;
        }


    }

    public static class UnitFilesChanged extends DBusSignal {


        public UnitFilesChanged(String _path) throws DBusException {
            super(_path);
        }



    }

    public static class Reloading extends DBusSignal {

        private final boolean active;

        public Reloading(String _path, boolean _active) throws DBusException {
            super(_path, _active);
            this.active = _active;
        }


        public boolean getActive() {
            return active;
        }


    }
}