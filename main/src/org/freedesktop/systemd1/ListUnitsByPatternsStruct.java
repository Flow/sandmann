package org.freedesktop.systemd1;

import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.Struct;
import org.freedesktop.dbus.annotations.Position;
import org.freedesktop.dbus.types.UInt32;

/**
 * Auto-generated class.
 */
public class ListUnitsByPatternsStruct extends Struct {
    @Position(0)
    private final String unitName;
    @Position(1)
    private final String description;
    @Position(2)
    private final String loadState;
    @Position(3)
    private final String activeState;
    @Position(4)
    private final String subState;
    @Position(5)
    private final String followingUnit;
    @Position(6)
    private final DBusPath objectPath;
    @Position(7)
    private final UInt32 jobId;
    @Position(8)
    private final String jobType;
    @Position(9)
    private final DBusPath jobObject;

    public ListUnitsByPatternsStruct(String unitName, String description, String loadState, String activeState, String subState, String followingUnit, DBusPath objectPath, UInt32 jobId, String jobType, DBusPath jobObject) {
        this.unitName = unitName;
        this.description = description;
        this.loadState = loadState;
        this.activeState = activeState;
        this.subState = subState;
        this.followingUnit = followingUnit;
        this.objectPath = objectPath;
        this.jobId = jobId;
        this.jobType = jobType;
        this.jobObject = jobObject;
    }


    public String getUnitName() {
        return unitName;
    }

    public String getDescription() {
        return description;
    }

    public String getLoadState() {
        return loadState;
    }

    public String getActiveState() {
        return activeState;
    }

    public String getSubState() {
        return subState;
    }

    public String getFollowingUnit() {
        return followingUnit;
    }

    public DBusPath getObjectPath() {
        return objectPath;
    }

    public UInt32 getJobId() {
        return jobId;
    }

    public String getJobType() {
        return jobType;
    }

    public DBusPath getJobObject() {
        return jobObject;
    }


}
