package org.freedesktop.systemd1;

import java.util.List;
import org.freedesktop.dbus.Tuple;
import org.freedesktop.dbus.annotations.Position;

/**
 * Auto-generated class.
 */
public class DisableUnitFilesWithFlagsAndInstallInfoTuple extends Tuple {
    @Position(0)
    private boolean carriesInstallInfo;
    @Position(1)
    private List<DisableUnitFilesWithFlagsAndInstallInfoStruct> changes;

    public DisableUnitFilesWithFlagsAndInstallInfoTuple(boolean carriesInstallInfo, List<DisableUnitFilesWithFlagsAndInstallInfoStruct> changes) {
        this.carriesInstallInfo = carriesInstallInfo;
        this.changes = changes;
    }

    public void setCarriesInstallInfo(boolean arg) {
        carriesInstallInfo = arg;
    }

    public boolean getCarriesInstallInfo() {
        return carriesInstallInfo;
    }
    public void setChanges(List<DisableUnitFilesWithFlagsAndInstallInfoStruct> arg) {
        changes = arg;
    }

    public List<DisableUnitFilesWithFlagsAndInstallInfoStruct> getChanges() {
        return changes;
    }


}