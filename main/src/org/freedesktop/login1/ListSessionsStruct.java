package org.freedesktop.login1;

import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.Struct;
import org.freedesktop.dbus.annotations.Position;
import org.freedesktop.dbus.types.UInt32;

/**
 * Auto-generated class.
 */
public class ListSessionsStruct extends Struct {
    @Position(0)
    private final String sessionId;
    @Position(1)
    private final UInt32 userId;
    @Position(2)
    private final String userName;
    @Position(3)
    private final String seatId;
    @Position(4)
    private final DBusPath sessionObjectPath;

    public ListSessionsStruct(String sessionId, UInt32 userId, String userName, String seatId, DBusPath sessionObjectPath) {
        this.sessionId = sessionId;
        this.userId = userId;
        this.userName = userName;
        this.seatId = seatId;
        this.sessionObjectPath = sessionObjectPath;
    }


    public String getSessionId() {
        return sessionId;
    }

    public UInt32 getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getSeatId() {
        return seatId;
    }

    public DBusPath getSessionObjectPath() {
        return sessionObjectPath;
    }


}
