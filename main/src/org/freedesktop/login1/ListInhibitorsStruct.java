package org.freedesktop.login1;

import org.freedesktop.dbus.Struct;
import org.freedesktop.dbus.annotations.Position;
import org.freedesktop.dbus.types.UInt32;

/**
 * Auto-generated class.
 */
public class ListInhibitorsStruct extends Struct {
    @Position(0)
    private final String what;
    @Position(1)
    private final String who;
    @Position(2)
    private final String why;
    @Position(3)
    private final String mode;
    @Position(4)
    private final UInt32 uid;
    @Position(5)
    private final UInt32 pid;

    public ListInhibitorsStruct(String what, String who, String why, String mode, UInt32 uid, UInt32 pid) {
        this.what = what;
        this.who = who;
        this.why = why;
        this.mode = mode;
        this.uid = uid;
        this.pid = pid;
    }


    public String getWhat() {
        return what;
    }

    public String getWho() {
        return who;
    }

    public String getWhy() {
        return why;
    }

    public String getMode() {
        return mode;
    }

    public UInt32 getUid() {
        return uid;
    }

    public UInt32 getPid() {
        return pid;
    }


}
