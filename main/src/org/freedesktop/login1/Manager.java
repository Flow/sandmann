package org.freedesktop.login1;

import java.util.List;
import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.FileDescriptor;
import org.freedesktop.dbus.TypeRef;
import org.freedesktop.dbus.annotations.DBusProperty;
import org.freedesktop.dbus.annotations.DBusProperty.Access;
import org.freedesktop.dbus.exceptions.DBusException;
import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.messages.DBusSignal;
import org.freedesktop.dbus.types.UInt32;
import org.freedesktop.dbus.types.UInt64;

/**
 * Auto-generated class.
 */
@DBusProperty(name = "EnableWallMessages", type = Boolean.class, access = Access.READ_WRITE)
@DBusProperty(name = "WallMessage", type = String.class, access = Access.READ_WRITE)
@DBusProperty(name = "NAutoVTs", type = UInt32.class, access = Access.READ)
@DBusProperty(name = "KillOnlyUsers", type = Manager.PropertyKillOnlyUsersType.class, access = Access.READ)
@DBusProperty(name = "KillExcludeUsers", type = Manager.PropertyKillExcludeUsersType.class, access = Access.READ)
@DBusProperty(name = "KillUserProcesses", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "RebootParameter", type = String.class, access = Access.READ)
@DBusProperty(name = "RebootToFirmwareSetup", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "RebootToBootLoaderMenu", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "RebootToBootLoaderEntry", type = String.class, access = Access.READ)
@DBusProperty(name = "BootLoaderEntries", type = Manager.PropertyBootLoaderEntriesType.class, access = Access.READ)
@DBusProperty(name = "IdleHint", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "IdleSinceHint", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "IdleSinceHintMonotonic", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "BlockInhibited", type = String.class, access = Access.READ)
@DBusProperty(name = "DelayInhibited", type = String.class, access = Access.READ)
@DBusProperty(name = "InhibitDelayMaxUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "UserStopDelayUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "HandlePowerKey", type = String.class, access = Access.READ)
@DBusProperty(name = "HandlePowerKeyLongPress", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleRebootKey", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleRebootKeyLongPress", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleSuspendKey", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleSuspendKeyLongPress", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleHibernateKey", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleHibernateKeyLongPress", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleLidSwitch", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleLidSwitchExternalPower", type = String.class, access = Access.READ)
@DBusProperty(name = "HandleLidSwitchDocked", type = String.class, access = Access.READ)
@DBusProperty(name = "HoldoffTimeoutUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "IdleAction", type = String.class, access = Access.READ)
@DBusProperty(name = "IdleActionUSec", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "PreparingForShutdown", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "PreparingForSleep", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "ScheduledShutdown", type = PropertyScheduledShutdownStruct.class, access = Access.READ)
@DBusProperty(name = "Docked", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "LidClosed", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "OnExternalPower", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "RemoveIPC", type = Boolean.class, access = Access.READ)
@DBusProperty(name = "RuntimeDirectorySize", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "RuntimeDirectoryInodesMax", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "InhibitorsMax", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "NCurrentInhibitors", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "SessionsMax", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "NCurrentSessions", type = UInt64.class, access = Access.READ)
@DBusProperty(name = "StopIdleSessionUSec", type = UInt64.class, access = Access.READ)
public interface Manager extends DBusInterface {


    public DBusPath GetSession(String sessionId);
    public DBusPath GetSessionByPID(UInt32 pid);
    public DBusPath GetUser(UInt32 uid);
    public DBusPath GetUserByPID(UInt32 pid);
    public DBusPath GetSeat(String seatId);
    public List<ListSessionsStruct> ListSessions();
    public List<ListUsersStruct> ListUsers();
    public List<ListSeatsStruct> ListSeats();
    public List<ListInhibitorsStruct> ListInhibitors();
    public CreateSessionTuple CreateSession(UInt32 uid, UInt32 pid, String service, String type, String classparam, String desktop, String seatId, UInt32 vtnr, String tty, String display, boolean remote, String remoteUser, String remoteHost, List<CreateSessionStruct> properties);
    public void ReleaseSession(String sessionId);
    public void ActivateSession(String sessionId);
    public void ActivateSessionOnSeat(String sessionId, String seatId);
    public void LockSession(String sessionId);
    public void UnlockSession(String sessionId);
    public void LockSessions();
    public void UnlockSessions();
    public void KillSession(String sessionId, String who, int signalNumber);
    public void KillUser(UInt32 uid, int signalNumber);
    public void TerminateSession(String sessionId);
    public void TerminateUser(UInt32 uid);
    public void TerminateSeat(String seatId);
    public void SetUserLinger(UInt32 uid, boolean enable, boolean interactive);
    public void AttachDevice(String seatId, String sysfsPath, boolean interactive);
    public void FlushDevices(boolean interactive);
    public void PowerOff(boolean interactive);
    public void PowerOffWithFlags(UInt64 flags);
    public void Reboot(boolean interactive);
    public void RebootWithFlags(UInt64 flags);
    public void Halt(boolean interactive);
    public void HaltWithFlags(UInt64 flags);
    public void Suspend(boolean interactive);
    public void SuspendWithFlags(UInt64 flags);
    public void Hibernate(boolean interactive);
    public void HibernateWithFlags(UInt64 flags);
    public void HybridSleep(boolean interactive);
    public void HybridSleepWithFlags(UInt64 flags);
    public void SuspendThenHibernate(boolean interactive);
    public void SuspendThenHibernateWithFlags(UInt64 flags);
    public String CanPowerOff();
    public String CanReboot();
    public String CanHalt();
    public String CanSuspend();
    public String CanHibernate();
    public String CanHybridSleep();
    public String CanSuspendThenHibernate();
    public void ScheduleShutdown(String type, UInt64 usec);
    public boolean CancelScheduledShutdown();
    public FileDescriptor Inhibit(String what, String who, String why, String mode);
    public String CanRebootParameter();
    public void SetRebootParameter(String parameter);
    public String CanRebootToFirmwareSetup();
    public void SetRebootToFirmwareSetup(boolean enable);
    public String CanRebootToBootLoaderMenu();
    public void SetRebootToBootLoaderMenu(UInt64 timeout);
    public String CanRebootToBootLoaderEntry();
    public void SetRebootToBootLoaderEntry(String bootLoaderEntry);
    public void SetWallMessage(String wallMessage, boolean enable);


    public static interface PropertyKillOnlyUsersType extends TypeRef<List<String>> {




    }

    public static interface PropertyKillExcludeUsersType extends TypeRef<List<String>> {




    }

    public static interface PropertyBootLoaderEntriesType extends TypeRef<List<String>> {




    }

    public static class SessionNew extends DBusSignal {

        private final String sessionId;
        private final DBusPath objectPath;

        public SessionNew(String _path, String _sessionId, DBusPath _objectPath) throws DBusException {
            super(_path, _sessionId, _objectPath);
            this.sessionId = _sessionId;
            this.objectPath = _objectPath;
        }


        public String getSessionId() {
            return sessionId;
        }

        public DBusPath getObjectPath() {
            return objectPath;
        }


    }

    public static class SessionRemoved extends DBusSignal {

        private final String sessionId;
        private final DBusPath objectPath;

        public SessionRemoved(String _path, String _sessionId, DBusPath _objectPath) throws DBusException {
            super(_path, _sessionId, _objectPath);
            this.sessionId = _sessionId;
            this.objectPath = _objectPath;
        }


        public String getSessionId() {
            return sessionId;
        }

        public DBusPath getObjectPath() {
            return objectPath;
        }


    }

    public static class UserNew extends DBusSignal {

        private final UInt32 uid;
        private final DBusPath objectPath;

        public UserNew(String _path, UInt32 _uid, DBusPath _objectPath) throws DBusException {
            super(_path, _uid, _objectPath);
            this.uid = _uid;
            this.objectPath = _objectPath;
        }


        public UInt32 getUid() {
            return uid;
        }

        public DBusPath getObjectPath() {
            return objectPath;
        }


    }

    public static class UserRemoved extends DBusSignal {

        private final UInt32 uid;
        private final DBusPath objectPath;

        public UserRemoved(String _path, UInt32 _uid, DBusPath _objectPath) throws DBusException {
            super(_path, _uid, _objectPath);
            this.uid = _uid;
            this.objectPath = _objectPath;
        }


        public UInt32 getUid() {
            return uid;
        }

        public DBusPath getObjectPath() {
            return objectPath;
        }


    }

    public static class SeatNew extends DBusSignal {

        private final String seatId;
        private final DBusPath objectPath;

        public SeatNew(String _path, String _seatId, DBusPath _objectPath) throws DBusException {
            super(_path, _seatId, _objectPath);
            this.seatId = _seatId;
            this.objectPath = _objectPath;
        }


        public String getSeatId() {
            return seatId;
        }

        public DBusPath getObjectPath() {
            return objectPath;
        }


    }

    public static class SeatRemoved extends DBusSignal {

        private final String seatId;
        private final DBusPath objectPath;

        public SeatRemoved(String _path, String _seatId, DBusPath _objectPath) throws DBusException {
            super(_path, _seatId, _objectPath);
            this.seatId = _seatId;
            this.objectPath = _objectPath;
        }


        public String getSeatId() {
            return seatId;
        }

        public DBusPath getObjectPath() {
            return objectPath;
        }


    }

    public static class PrepareForShutdown extends DBusSignal {

        private final boolean start;

        public PrepareForShutdown(String _path, boolean _start) throws DBusException {
            super(_path, _start);
            this.start = _start;
        }


        public boolean getStart() {
            return start;
        }


    }

    public static class PrepareForSleep extends DBusSignal {

        private final boolean start;

        public PrepareForSleep(String _path, boolean _start) throws DBusException {
            super(_path, _start);
            this.start = _start;
        }


        public boolean getStart() {
            return start;
        }


    }
}