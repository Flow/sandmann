package org.freedesktop.login1;

import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.FileDescriptor;
import org.freedesktop.dbus.Tuple;
import org.freedesktop.dbus.annotations.Position;
import org.freedesktop.dbus.types.UInt32;

/**
 * Auto-generated class.
 */
public class CreateSessionTuple extends Tuple {
    @Position(0)
    private String sessionId;
    @Position(1)
    private DBusPath objectPath;
    @Position(2)
    private String runtimePath;
    @Position(3)
    private FileDescriptor fifoFd;
    @Position(4)
    private UInt32 uid;
    @Position(5)
    private String seatId;
    @Position(6)
    private UInt32 vtnr;
    @Position(7)
    private boolean existing;

    public CreateSessionTuple(String sessionId, DBusPath objectPath, String runtimePath, FileDescriptor fifoFd, UInt32 uid, String seatId, UInt32 vtnr, boolean existing) {
        this.sessionId = sessionId;
        this.objectPath = objectPath;
        this.runtimePath = runtimePath;
        this.fifoFd = fifoFd;
        this.uid = uid;
        this.seatId = seatId;
        this.vtnr = vtnr;
        this.existing = existing;
    }

    public void setSessionId(String arg) {
        sessionId = arg;
    }

    public String getSessionId() {
        return sessionId;
    }
    public void setObjectPath(DBusPath arg) {
        objectPath = arg;
    }

    public DBusPath getObjectPath() {
        return objectPath;
    }
    public void setRuntimePath(String arg) {
        runtimePath = arg;
    }

    public String getRuntimePath() {
        return runtimePath;
    }
    public void setFifoFd(FileDescriptor arg) {
        fifoFd = arg;
    }

    public FileDescriptor getFifoFd() {
        return fifoFd;
    }
    public void setUid(UInt32 arg) {
        uid = arg;
    }

    public UInt32 getUid() {
        return uid;
    }
    public void setSeatId(String arg) {
        seatId = arg;
    }

    public String getSeatId() {
        return seatId;
    }
    public void setVtnr(UInt32 arg) {
        vtnr = arg;
    }

    public UInt32 getVtnr() {
        return vtnr;
    }
    public void setExisting(boolean arg) {
        existing = arg;
    }

    public boolean getExisting() {
        return existing;
    }


}