package org.freedesktop.login1;

import org.freedesktop.dbus.DBusPath;
import org.freedesktop.dbus.Struct;
import org.freedesktop.dbus.annotations.Position;
import org.freedesktop.dbus.types.UInt32;

/**
 * Auto-generated class.
 */
public class ListUsersStruct extends Struct {
    @Position(0)
    private final UInt32 id;
    @Position(1)
    private final String username;
    @Position(2)
    private final DBusPath objectPath;

    public ListUsersStruct(UInt32 id, String username, DBusPath objectPath) {
        this.id = id;
        this.username = username;
        this.objectPath = objectPath;
    }


    public UInt32 getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public DBusPath getObjectPath() {
        return objectPath;
    }

}
