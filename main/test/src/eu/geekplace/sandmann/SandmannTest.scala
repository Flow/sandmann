// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023 Florian Schmaus
package eu.geekplace.sandmann

import utest.*

object SandmannTest extends TestSuite:
  val tests = Tests {
    test("no-op test") {
      42
    }
  }
