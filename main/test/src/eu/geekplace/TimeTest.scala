// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023-2024 Florian Schmaus
package eu.geekplace

import java.time.Instant

import utest.*

object TimeTest extends TestSuite:
  import Time.*
  val tests = Tests {
    test("realtimeUsecsToInstant(1694130316144780)") {
      val res = realtimeUsecsToInstant(1694130316144780L)
      res ==> Instant.parse("2023-09-07T23:45:16.144780Z")
    }
  }
