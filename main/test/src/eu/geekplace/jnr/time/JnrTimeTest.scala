// SPDX-License-Identifier: GPL-3.0-or-later
// Copyright © 2023-2024 Florian Schmaus
package eu.geekplace.jnr.time

import jnr.ffi.Struct
import utest.*

object TimeTest extends TestSuite:
  val tests = Tests {
    test("Itimerspec") {
      val value = Timespec(1)
      val itimerspec = Itimerspec(value = value)
      val size = Struct.size(itimerspec)
      size ==> 32
      val memory = Struct.getMemory(itimerspec)

      memory.getLong(0) ==> 0
      memory.getLong(8) ==> 0
      memory.getLong(16) ==> 1
      memory.getLong(24) ==> 0

      itimerspec.interval.seconds.set(1)
      memory.getLong(0) ==> 1
      memory.getLong(8) ==> 0
      memory.getLong(16) ==> 1
      memory.getLong(24) ==> 0
    }
  }
