MILL_BIN ?= ./millw
# Can be one of jar or native.
DEPLOY_TYPE ?= jar

.PHONY: compile
compile:
	$(MILL_BIN) __.compile

.PHONY: assembly
assembly:
	$(MILL_BIN) __.assembly

.PHONY: check
check: check-format scalafix test check-license

.PHONY: check-format
check-format:
	$(MILL_BIN) __.checkFormat

.PHONY: check-license
check-license:
	./tools/check-license

.PHONY: format
format:
	$(MILL_BIN) __.reformat

.PHONY: test
test:
	$(MILL_BIN) __.test

.PHONY: scalafix
scalafix:
	$(MILL_BIN) __.fix

.PHONY: show-dependency-updates
show-dependency-updates:
	 $(MILL_BIN) mill.scalalib.Dependency/showUpdates

.PHONY: show-dependency-tree
show-dependency-tree:
	 $(MILL_BIN) __.ivyDepsTree

.PHONY: show-dependencies
show-dependencies:
	 $(MILL_BIN) show __.resolvedIvyDeps

FAT_JAR := out/main/assembly.dest/out.jar
NATIVE_IMAGE := out/main/nativeImage.dest/sandmann

CONFIG_FILE := sandmann.example.yaml
SYSTEMD_SERVICE := sandmann.service
TMPFILES_CONF := sandmann.tmpfiles.conf
SYSUSERS_CONF := sandmann.sysusers.conf
POLKIT_RULES := sandmann.polkit.rules
DEST_JAR := usr/lib/sandmann/sandmann.jar
DEST_NATIVE_IMAGE := usr/libexec/sandmann

SYSTEMD_SYSTEM_UNIT_DIR := /usr/lib/systemd/system
TMPFILES_DIR := /usr/lib/tmpfiles.d
SYSUSERS_DIR := /usr/lib/sysusers.d
POLKIT_RULES_DIR = /usr/share/polkit-1/rules.d
LIBEXEC_DIR := /usr/libexec/sandmann

TARGET_DEST_JAR := $(DESTDIR)/$(DEST_JAR)
TARGET_DEST_NATIVE_IMAGE := $(DESTDIR)/$(DEST_NATIVE_IMAGE)
TARGET_SYSTEMD_SYSTEM_UNIT_DIR := $(DESTDIR)/$(SYSTEMD_SYSTEM_UNIT_DIR)
TARGET_SYSTEMD_SYSTEM_UNIT := $(TARGET_SYSTEMD_SYSTEM_UNIT_DIR)/sandmann.service
TARGET_TMPFILES_DIR := $(DESTDIR)/$(TMPFILES_DIR)
TARGET_TMPFILES_CONF := $(TARGET_TMPFILES_DIR)/sandmann.conf
TARGET_SYSUSERS_DIR := $(DESTDIR)/$(SYSUSERS_DIR)
TARGET_SYSUSERS_CONF := $(TARGET_SYSUSERS_DIR)/sandmann.conf
TARGET_POLKIT_RULES_DIR := $(DESTDIR)/$(POLKIT_RULES_DIR)
TARGET_POLKIT_RULES := $(TARGET_POLKIT_RULES_DIR)/sandmann.rules
TARGET_LIBEXEC_DIR := $(DESTDIR)/$(LIBEXEC_DIR)
TARGET_SAND_REPORT_ACTIVE := $(TARGET_LIBEXEC_DIR)/sand-report-active
TARGET_SAND_REPORT_IDLE := $(TARGET_LIBEXEC_DIR)/sand-report-idle
TARGET_SAND_REPORT_INHIBIT_IDLE := $(TARGET_LIBEXEC_DIR)/sand-report-inhibit-idle

ifeq ($(MAYBE_SUDO), sudo)
INSTALL_ARGS += -o root -g root
endif
INSTALL := $(MAYBE_SUDO) install -D $(INSTALL_ARGS)

ifeq ($(SOURCELESS_INSTALL), true)
SOURCE_DEPS :=
else
SOURCE_DEPS := build.sc $(shell find main -type f)
endif

$(FAT_JAR): $(SOURCE_DEPS)
	$(MILL_BIN) main.assembly

$(NATIVE_IMAGE): $(SOURCE_DEPS)
	$(MILL_BIN) main.nativeImage

$(TARGET_DEST_JAR): $(FAT_JAR)
	$(INSTALL) -m444 $^ $@

$(TARGET_DEST_NATIVE_IMAGE): $(NATIVE_IMAGE)
	$(INSTALL) -m755 $^ $@

$(TARGET_TMPFILES_CONF): $(TMPFILES_CONF)
	$(INSTALL) -m444 $^ $@

$(TARGET_SAND_REPORT_ACTIVE): libexec/sand-report-active
	$(INSTALL) -m755 $^ $@

$(TARGET_SAND_REPORT_IDLE): libexec/sand-report-idle
	$(INSTALL) -m755 $^ $@

$(TARGET_SAND_REPORT_INHIBIT_IDLE): libexec/sand-report-inhibit-idle
	$(INSTALL) -m755 $^ $@

sandmann.native.service: $(SYSTEMD_SERVICE)
	sed "s;^ExecStart=.*;ExecStart=/$(DEST_NATIVE_IMAGE);" $^ > $@

.PHONY: native
native: $(NATIVE_IMAGE) sandmann.native.service

ifeq ($(DEPLOY_TYPE), jar)
SOURCE_SYSTEMD_SERVICE := $(SYSTEMD_SERVICE)
TARGET_BINARY := $(TARGET_DEST_JAR)
else ifeq ($(DEPLOY_TYPE), native)
$(warning WARNING: Native images of Sandmann do not (yet) work)
SOURCE_SYSTEMD_SERVICE := sandmann.native.service
TARGET_BINARY := $(TARGET_DEST_NATIVE_IMAGE)
else
$(error Unknown DEPLOY_TYPE: $(DEPLOY_TYPE)
endif

$(TARGET_SYSTEMD_SYSTEM_UNIT): $(SOURCE_SYSTEMD_SERVICE)
	$(INSTALL) -m444 $^ $@

.PHONY: install
install: $(TARGET_BINARY) $(TARGET_SYSTEMD_SYSTEM_UNIT) $(TARGET_TMPFILES_CONF) $(TARGET_SAND_REPORT_ACTIVE) $(TARGET_SAND_REPORT_IDLE) $(TARGET_SAND_REPORT_INHIBIT_IDLE)
	$(INSTALL) -m444 $(POLKIT_RULES) $(TARGET_POLKIT_RULES)

.PHONY: sudo-install
sudo-install:
	$(MAKE) install MAYBE_SUDO=sudo

.PHONY: deploy
deploy: sudo-install
	sudo systemctl daemon-reload
	sudo systemctl restart sandmann

$(TARGET_SYSUSERS_CONF): $(SYSUSERS_CONF)
	$(INSTALL) -m444 $^ $@

.PHONY: full-deploy
full-deploy: $(TARGET_SYSUSERS_CONF)
	$(MAKE) deploy

.PHONY: clean
clean:
	rm -rf out
