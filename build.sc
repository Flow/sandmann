import $ivy.`com.goyeau::mill-scalafix::0.4.2`
import $ivy.`io.github.alexarchambault.mill::mill-native-image::0.1.25`
import com.goyeau.mill.scalafix.ScalafixModule
import mill._, scalalib._, scalafmt._

object main extends ScalaModule
    with ScalafmtModule with ScalafixModule
    with io.github.alexarchambault.millnativeimage.NativeImage {
  def scalaVersion = "3.5.1"

  def scalacOptions = super.scalacOptions() ++ Seq(
    "-new-syntax",
    "-rewrite",
    "-deprecation",
    "-explain",
    "-Wunused:imports",
    "-Werror",
  )

  def javacOptions = super.javacOptions() ++ Seq(
    "--release", "17",
  )

  // log4j is a multi-release Jar file. Declare the assembled uber Jar
  // also a multi-release Jar file to avoid
  // https://github.com/apache/logging-log4j2/pull/1760#pullrequestreview-1612846910
  def manifest = super.manifest().add(
    "Multi-Release" -> "true"
  )

  val dbusJavaVersion = "5.1.0"
  val log4jVersion = "2.24.1"

  def ivyDeps = Agg(
    ivy"com.github.hypfvieh:dbus-java-core:$dbusJavaVersion",
    ivy"com.github.hypfvieh:dbus-java-transport-jnr-unixsocket:$dbusJavaVersion",
//    ivy"com.github.hypfvieh:dbus-java-transport-native-unixsocket:$dbusJavaVersion",
    ivy"com.github.jnr:jnr-ffi:2.2.17",
    ivy"com.github.jnr:jnr-posix:3.1.20",
    ivy"com.lihaoyi::os-lib:0.11.3",
    ivy"com.rm5248:dbus-java-nativefd:2.0",
    ivy"de.bwaldvogel:log4j-systemd-journal-appender:2.5.1",
    ivy"dev.dirs:directories:26",
    ivy"org.apache.logging.log4j::log4j-api-scala:13.1.0",
    ivy"org.apache.logging.log4j:log4j-api:$log4jVersion",
    ivy"org.apache.logging.log4j:log4j-core:$log4jVersion",
    ivy"org.apache.logging.log4j:log4j-slf4j2-impl:$log4jVersion",
    ivy"org.rogach::scallop:5.1.0",
    ivy"org.virtuslab::scala-yaml:0.3.0",
  )

  def gitVersion = T.input {
    os.proc("git", "describe", "--always", "--tags", "--dirty=+").call(cwd = T.workspace).out.text()
  }

  def gitVersionFileResourceDir = T {
    val dest = T.dest / "eu.geekplace.sandmann" / "gitVersion"
    os.write(dest, gitVersion(), createFolders = true)
    PathRef(T.dest)
  }

  def scalaVersionFileResourceDir = T {
    val dest = T.dest / "eu.geekplace.sandmann" / "scalaVersion"
    os.write(dest, scalaVersion(), createFolders = true)
    PathRef(T.dest)
  }

  override def resources = T.sources { super.resources() :+ gitVersionFileResourceDir() :+ scalaVersionFileResourceDir() }

  def nativeImageName = "sandmann"
  def nativeImageMainClass = "eu.geekplace.sandmann.Sandmann"
  def nativeImageClassPath = runClasspath()
  def nativeImageGraalVmJvmId = "graalvm-java17:22.3.3"
  def nativeImageOptions = Seq(
    "--no-fallback",
  )

  object test extends ScalaTests with ScalafmtModule with TestModule.Utest {
    def ivyDeps = Agg(ivy"com.lihaoyi::utest:0.8.4")
  }

}
